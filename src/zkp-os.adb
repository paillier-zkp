--
--  Copyright (C) 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--  University of Applied Sciences Rapperswil
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Direct_IO;
with Ada.Directories;

package body ZKP.OS is

   -------------------------------------------------------------------------

   function Read_File (Filename : String) return String
   is
      Size : constant Natural := Natural (Ada.Directories.Size (Filename));

      subtype Content_String is String (1 .. Size);
      package FIO is new Ada.Direct_IO (Content_String);

      Input_File : FIO.File_Type;
      Content    : Content_String;
   begin
      FIO.Open (File => Input_File,
                Mode => FIO.In_File,
                Name => Filename,
                Form => "shared=yes");

      FIO.Read (File => Input_File,
                Item => Content);

      FIO.Close (File => Input_File);

      return Content;

   exception
      when others =>
         if FIO.Is_Open (File => Input_File) then
            FIO.Close (File => Input_File);
         end if;
         raise;
   end Read_File;

   -------------------------------------------------------------------------

   procedure Write_File
     (Content  : String;
      Filename : String)
   is
      subtype Content_String is String (Content'Range);
      package FIO is new Ada.Direct_IO (Content_String);

      Output_File : FIO.File_Type;
   begin
      FIO.Create (File => Output_File,
                  Name => Filename,
                  Form => "shared=no");

      FIO.Write (File => Output_File,
                 Item => Content);

      FIO.Close (File => Output_File);

   exception
      when others =>
         if FIO.Is_Open (File => Output_File) then
            FIO.Close (File => Output_File);
            Ada.Directories.Delete_File (Name => Filename);
         end if;
         raise;
   end Write_File;

end ZKP.OS;
