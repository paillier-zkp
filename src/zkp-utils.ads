--
--  Copyright (C) 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--  University of Applied Sciences Rapperswil
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with ZKP.Elections;

package ZKP.Utils is

   function To_Hex_String (Integer : GMP.Binding.Mpz_T) return String;
   --  Return hexadecimal representation of given GMP bignum.

   function To_Bignum (Hex_Str : String) return GMP.Binding.Mpz_T;
   --  Create GMP bignum from given hexadecimal string.

   function To_String
     (Name    : String;
      Numbers : Mpz_T_Array)
      return String;
   --  Convert given GMP bignum array to string. Use specified name as label.

   function To_String
     (E      : Elections.Election_Type;
      Voter  : String)
      return String;
   --  Return election information as string.

   function To_String
     (Candidates : Elections.Candidates_Type;
      Choice     : Natural)
      return String;
   --  Return candidate selection as string.

   Conversion_Error : exception;

end ZKP.Utils;
