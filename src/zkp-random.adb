--
--  Copyright (C) 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--  University of Applied Sciences Rapperswil
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Sequential_IO;

with Interfaces.C;

package body ZKP.Random is

   package IC renames Interfaces.C;

   Urandom_Path : constant String := "/dev/urandom";
   --  Path to our random source.

   Random_State : Gmp_Randstate_T;
   --  Random generator state.

   procedure Init;
   --  Initialize the random number generator.

   package S_IO is new Ada.Sequential_IO (Element_Type => IC.unsigned_long);

   function Get_Ui_From_Urandom return IC.unsigned_long;
   --  Create random unsigned number using /dev/urandom as random source.

   -------------------------------------------------------------------------

   function Get (Bit_Size : Natural) return Mpz_T
   is
   begin
      return R : Mpz_T do
         Mpz_Init (Integer => R);
         Mpz_Urandomb (Rop   => R,
                       State => Random_State,
                       N     => IC.unsigned_long (Bit_Size));
      end return;
   end Get;

   -------------------------------------------------------------------------

   function Get
     (Bit_Size : Natural;
      Count    : Positive)
      return Mpz_T_Array
   is
      Rs : Mpz_T_Array (0 .. Count - 1);
   begin
      for I in Rs'Range loop
         Rs (I) := Get (Bit_Size => Bit_Size);
      end loop;

      return Rs;
   end Get;

   -------------------------------------------------------------------------

   function Get_Coprime (Number : Mpz_T) return Mpz_T
   is
      use type IC.int;
      use type IC.unsigned_long;

      R, Gcd : Mpz_T;
   begin
      Mpz_Init (Integer => Gcd);

      loop
         R := Get (Bit_Size => Natural
                   (Mpz_Sizeinbase
                      (Op   => Number,
                       Base => 2)));
         Mpz_Gcd (Rop => Gcd,
                  Op1 => Number,
                  Op2 => R);
         exit when Mpz_Get_Ui (Op => Gcd) = 1 and Mpz_Cmp
           (Op1 => R, Op2 => Number) <= 0;
         Mpz_Clear (Integer => R);
      end loop;

      Mpz_Clear (Integer => Gcd);

      return R;
   end Get_Coprime;

   -------------------------------------------------------------------------

   function Get_Coprimes
     (Number : Mpz_T;
      Count  : Positive)
      return Mpz_T_Array
   is
      Coprimes : Mpz_T_Array (0 .. Count - 1);
   begin
      for C in Coprimes'Range loop
         Coprimes (C) := Get_Coprime (Number => Number);
      end loop;

      return Coprimes;
   end Get_Coprimes;

   -------------------------------------------------------------------------

   function Get_Ui_From_Urandom return IC.unsigned_long
   is
      use S_IO;

      Ui         : IC.unsigned_long;
      Input_File : File_Type;
   begin
      Open (File => Input_File,
            Mode => In_File,
            Name => Urandom_Path,
            Form => "shared=yes");

      Read (File => Input_File,
            Item => Ui);

      Close (File => Input_File);
      return Ui;

   exception
      when others =>
         if Is_Open (File => Input_File) then
            Close (File => Input_File);
         end if;
         raise Seed_Error with "Unable to seed random number generator";
   end Get_Ui_From_Urandom;

   -------------------------------------------------------------------------

   procedure Init
   is
   begin
      Gmp_Randinit_Default (State => Random_State);
      Gmp_Randseed_Ui (State => Random_State,
                       Seed  => Get_Ui_From_Urandom);
   end Init;

begin
   Init;
end ZKP.Random;
