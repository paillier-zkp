--
--  Copyright (C) 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--  University of Applied Sciences Rapperswil
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

package ZKP.JSON is

   E_ID_Fieldname       : constant String := "election_id";
   Desc_Fieldname       : constant String := "election_description";
   Candidates_Fieldname : constant String := "candidates_list";
   Voter_ID_Fieldname   : constant String := "voter_id";
   Ballot_Fieldname     : constant String := "ballot";
   Proof_Fieldname      : constant String := "proof";
   Voters_Fieldname     : constant String := "voters";
   Vote_Fieldname       : constant String := "vote";
   N_Fieldname          : constant String := "n";
   G_Fieldname          : constant String := "g";
   Exponent_Fieldname   : constant String := "base_exponent";
   A_Fieldname          : constant String := "a";
   E_Fieldname          : constant String := "e";
   Z_Fieldname          : constant String := "z";
   Tally_Fieldname      : constant String := "ct";
   Cheat_Fieldname      : constant String := "cheat";

end ZKP.JSON;
