--
--  Copyright (C) 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--  University of Applied Sciences Rapperswil
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Strings.Fixed;
with Ada.Strings.Unbounded;

with Interfaces.C;

package body ZKP.Utils is

   -------------------------------------------------------------------------

   function To_Bignum (Hex_Str : String) return GMP.Binding.Mpz_T
   is
      use type Interfaces.C.int;

      Number : GMP.Binding.Mpz_T;
      Res    : Interfaces.C.int;
   begin
      GMP.Binding.Mpz_Init_Set_Str
        (Result => Res,
         Rop    => Number,
         Str    => Interfaces.C.To_C (Hex_Str),
         Base   => 16);

      if Res /= 0 then
         GMP.Binding.Mpz_Clear (Integer => Number);
         raise Conversion_Error with "Not a valid base 16 number: " & Hex_Str;
      end if;

      return Number;
   end To_Bignum;

   -------------------------------------------------------------------------

   function To_Hex_String (Integer : GMP.Binding.Mpz_T) return String
   is
      Hex : Interfaces.C.char_array
        (0 .. GMP.Binding.Mpz_Sizeinbase (Op   => Integer,
                                          Base => 16));
   begin
      GMP.Binding.Mpz_Get_Str
        (Str  => Hex,
         Base => 16,
         Op   => Integer);
      return Interfaces.C.To_Ada (Hex);
   end To_Hex_String;

   -------------------------------------------------------------------------

   function To_String
     (Name    : String;
      Numbers : Mpz_T_Array)
      return String
   is
      use Ada.Strings;
      use Ada.Strings.Unbounded;

      Result : Unbounded_String;
   begin
      for K in Numbers'Range loop
         declare
            K_Str : constant String := Fixed.Trim
              (Source => K'Img,
               Side   => Left);
         begin
            Result := Result & Name & " (" & K_Str & ") "
              & Utils.To_Hex_String (Integer => Numbers (K));
            if K /= Numbers'Last then
               Result := Result & ASCII.LF;
            end if;
         end;
      end loop;

      return To_String (Result);
   end To_String;

   -------------------------------------------------------------------------

   function To_String
     (E      : Elections.Election_Type;
      Voter  : String)
      return String
   is
      use Ada.Strings.Unbounded;
      use ZKP.Elections;

      Result : Unbounded_String;
   begin
      Result := To_Unbounded_String ("* Election")           & ASCII.LF
        & "Identifier  : " & Get_ID (Election => E)          & ASCII.LF
        & "Description : " & Get_Description (Election => E) & ASCII.LF
        & "Voter       : " & Voter                           & ASCII.LF
        & "Modulus n   : " & Get_N (Election => E)           & ASCII.LF
        & "Exponent    : 2 ^" & Get_Exponent (Election => E)'Img;

      return To_String (Result);
   end To_String;

   -------------------------------------------------------------------------

   function To_String
     (Candidates : Elections.Candidates_Type;
      Choice     : Natural)
      return String
   is
      use Ada.Strings.Unbounded;

      Result : Unbounded_String;
   begin
      Result := Result & "Ballot      : ";
      for C in Candidates'Range loop
         Result := Result & "[";
         if C = Choice then
            Result := Result & "x";
         else
            Result := Result & " ";
         end if;
         Result := Result & "] " & Candidates (C);
         if C /= Candidates'Last then
            Result := Result & ", ";
         end if;
      end loop;

      return To_String (Result);
   end To_String;

end ZKP.Utils;
