--
--  Copyright (C) 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--  University of Applied Sciences Rapperswil
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with GNATCOLL.JSON;

with ZKP.OS;

package body ZKP.Registry is

   function U (S : String) return Unbounded_String renames To_Unbounded_String;

   procedure JSON_To_Registry
     (Registry : in out Shareholder_Registry_Type;
      Value    :        GNATCOLL.JSON.JSON_Value);
   --  Insert shareholder data stored in JSON value into registry.

   -------------------------------------------------------------------------

   procedure Add_Shareholder
     (Registry : in out Shareholder_Registry_Type;
      Name     :        String;
      Shares   :        Long_Integer)
   is
      U_Name : constant Unbounded_String := U (Name);
   begin
      if Registry.Data.Contains (Key => U_Name) then
         raise Shareholder_Present with "Shareholder " & Name
           & " already registered";
      end if;

      Registry.Data.Insert (Key      => U (Name),
                            New_Item => Shares);
   end Add_Shareholder;

   -------------------------------------------------------------------------

   function Contains
     (Registry : Shareholder_Registry_Type;
      Name     : String)
      return Boolean
   is
   begin
      return Registry.Data.Contains (Key => U (Name));
   end Contains;

   -------------------------------------------------------------------------

   function Get_Share_Count
     (Registry : Shareholder_Registry_Type;
      Name     : String)
      return Long_Integer
   is
      U_Name : constant Unbounded_String := U (Name);
   begin
      if Registry.Data.Contains (Key => U_Name) then
         return Registry.Data.Element (Key => U_Name);
      end if;

      raise Shareholder_Not_Present with "No entry for shareholder "
        & Name;
   end Get_Share_Count;

   -------------------------------------------------------------------------

   procedure JSON_To_Registry
     (Registry : in out Shareholder_Registry_Type;
      Value    :        GNATCOLL.JSON.JSON_Value)
   is
      use GNATCOLL.JSON;

      procedure Insert_Shareholder
        (Name  : UTF8_String;
         Value : JSON_Value);
      --  Insert Shareholder with given name and share count into registry.

      procedure Insert_Shareholder
        (Name  : UTF8_String;
         Value : JSON_Value)
      is
      begin
         Add_Shareholder
           (Registry => Registry,
            Name     => Name,
            Shares   => Long_Integer (Integer'(Get (Val => Value))));
      end Insert_Shareholder;
   begin
      Value.Map_JSON_Object (CB => Insert_Shareholder'Access);
   end JSON_To_Registry;

   -------------------------------------------------------------------------

   function Load_File (Filename : String) return Shareholder_Registry_Type
   is
      use GNATCOLL.JSON;

      Content : constant String     := OS.Read_File (Filename => Filename);
      Val     : constant JSON_Value := Read
        (Strm     => Content,
         Filename => Filename);
   begin
      return R : Shareholder_Registry_Type do
         JSON_To_Registry (Registry => R,
                           Value    => Val);
      end return;
   end Load_File;

   -------------------------------------------------------------------------

   function Size (Registry : Shareholder_Registry_Type) return Natural
   is
   begin
      return Natural (Registry.Data.Length);
   end Size;

end ZKP.Registry;
