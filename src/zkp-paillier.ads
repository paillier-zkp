--
--  Copyright (C) 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--  University of Applied Sciences Rapperswil
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

package ZKP.Paillier is

   use GMP.Binding;

   subtype Candidate_Count is Positive range 2 .. 64;
   --  Number of candidates in a vote. At least two candidates are needed
   --  ('None' and some other choice).

   type Mk_Array is new Mpz_T_Array;
   --  Array of valid voting messages mk.

   type Uk_Array is new Mpz_T_Array;
   --  Array of valid ballot variants without masking r.

   type Ak_Array is new Mpz_T_Array;
   --  Commitment array ak.

   type Ek_Array is new Mpz_T_Array;
   --  Response array ek.

   type Zk_Array is new Mpz_T_Array;
   --  Response array zk.

   function Create_Messages_Mk
     (N          : Mpz_T;
      Base       : Mpz_T;
      Candidates : Candidate_Count)
      return Mk_Array;
   --  Create array of valid voting messages mk for given n, base and number of
   --  candidates [m_0,m_1,m_2,...,m_candidates-1]. The resulting array index
   --  range is 0 .. candidates - 1.

   function Compute_Uk
     (Mk : Mk_Array;
      G  : Mpz_T;
      N2 : Mpz_T;
      C  : Mpz_T)
      return Uk_Array;
   --  Calculate all valid ballot variants (uk) for given voting messages mk,
   --  g, n^2 and the encrypted vote c.

   function Compute_Commitment_Ak
     (Choice : Natural;
      Uk     : Uk_Array;
      Ek     : Mpz_T_Array;
      Zk     : Mpz_T_Array;
      N      : Mpz_T;
      N2     : Mpz_T)
      return Ak_Array;
   --  Calculate commitment array (ak) for given Choice (the message selected).
   --  ek is the array of random integers ek < 2^b for k /= i; zk is the array
   --  of random integers zk from the set of integers coprime to n.
   --  uk is the array of valid ballots without masking r calculated using the
   --  Calculate_Uk function.

   subtype Challenge_Index is Positive range 256 .. Positive'Last;
   --  Allowed bit lengths of challenge. At least 256 bits are required.

   function Compute_Challenge
     (Election_Id : String;
      Voter_Id    : String;
      C           : Mpz_T;
      Commitment  : Ak_Array;
      Size        : Challenge_Index)
      return Mpz_T;
   --  Compute a ZKP challenge using the election and voter IDs, the encrypted
   --  vote c and the commitment array ak. The challenge is calculated by
   --  hashing the given arguments with SHA256; the initial SHA256 hash is
   --  constructed as follows:
   --
   --  Hash(m) = H(Election_Id || Voter_Id || hex(c) || hex(ak(0)) || ...
   --              || hex(ak(k)))
   --
   --  The Size argument defines the bit size of the resulting challenge, it
   --  should be a multiple of 256 (if not, the result will be rounded up/down
   --  to a multiple of 256; e.g. a Size of 257 will result in a challenge size
   --  of <= 255 bits. The resulting bit size of the challenge is guaranteed to
   --  be smaller than the requested size.
   --
   --  To get the requested bit size from the initial 256 bit hash, it is
   --  expanded by re-hashing it 'Size / 256' times (using the same SHA256
   --  context). The resulting message digests are concatenated with the
   --  initial hash to form the complete challenge.

   function Compute_Response_Ek
     (Choice    : Natural;
      Challenge : Mpz_T;
      Size      : Challenge_Index;
      Random_E  : Mpz_T_Array)
      return Ek_Array;
   --  Compute response array ek for given Choice (the message selected) using
   --  the specified Challenge, bit Size and random numbers e.

   function Compute_Response_Zk
     (Choice   : Natural;
      N        : Mpz_T;
      R        : Mpz_T;
      Random_Z : Mpz_T_Array;
      Ek       : Ek_Array)
      return Zk_Array;
   --  Compute response array zk for given Choice (the message selected) using
   --  the specified modulus n, masking random r, array of random numbers z and
   --  the ek array calculated using the Compute_Response_Ek function.

   function Verify_Response_Ek
     (Challenge : Mpz_T;
      Ek        : Ek_Array)
      return Boolean;
   --  Verify if the given challenge equals the sum of the values in the
   --  specified response ek array: sum (ek) = challenge mod 2^b.

   procedure Verify_Nth_Powers
     (Success : out Boolean;
      Err_Idx : out Integer;
      Uk      :     Uk_Array;
      Ak      :     Ak_Array;
      Ek      :     Ek_Array;
      Zk      :     Zk_Array;
      N       :     Mpz_T);
   --  Verify n-th powers: zk^n = ak * uk^ek mod n^2.
   --
   --  The result of the verification is returned in the Success parameter. The
   --  Err_Idx parameter points to the (first) incorrect z value if the n-th
   --  powers could not be verified; Err_Idx is set to -1 on success.

   function Encrypt
     (M  : Mpz_T;
      R  : Mpz_T;
      G  : Mpz_T;
      N  : Mpz_T;
      N2 : Mpz_T)
      return Mpz_T;
   --  Perform Paillier encryption of given message m using the specified
   --  crypto system parameters.

   Calculation_Error : exception;

end ZKP.Paillier;
