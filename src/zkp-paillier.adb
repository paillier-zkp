--
--  Copyright (C) 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--  University of Applied Sciences Rapperswil
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Interfaces.C;

with GNAT.SHA256;

with ZKP.Utils;

package body ZKP.Paillier is

   package IC renames Interfaces.C;

   procedure Compute_Hash
     (SHA_Context : in out GNAT.SHA256.Context;
      Election_Id :        String;
      Voter_Id    :        String;
      C           :        Mpz_T;
      Commitment  :        Ak_Array);
   --  Compute SHA256 hash of election and voter IDs, the encrypted vote c and
   --  the commitment array ak. The resulting hash is stored in the given
   --  SHA256 context.

   -------------------------------------------------------------------------

   function Compute_Challenge
     (Election_Id : String;
      Voter_Id    : String;
      C           : Mpz_T;
      Commitment  : Ak_Array;
      Size        : Challenge_Index)
      return Mpz_T
   is
      use GNAT;

      Rounds   : constant Integer
        := Integer (Float'Rounding (Float (Size) / 256.0));
      Expanded : String (1 .. Rounds * 64) := (others => ' ');
      Hasher   : SHA256.Context            := SHA256.Initial_Context;
   begin
      Compute_Hash (SHA_Context => Hasher,
                    Election_Id => Election_Id,
                    Voter_Id    => Voter_Id,
                    C           => C,
                    Commitment  => Commitment);

      Expanded (1 .. 64) := SHA256.Digest (C => Hasher);

      --  The first round is already done here: -1.

      for Round in Integer range 1 .. Rounds - 1 loop

         Hasher := SHA256.Initial_Context;

         --  Update SHA256 context with previous hash.

         SHA256.Update
           (C     => Hasher,
            Input => Expanded ((Round - 1) * 64 + 1 .. Round * 64));

         --  Append digest to result.

         Expanded (Round * 64 + 1 .. (Round + 1) * 64)
           := SHA256.Digest (C => Hasher);
      end loop;

      declare
         Challenge : Mpz_T := Utils.To_Bignum (Hex_Str => Expanded);
      begin

         --  Clear leading bit to ensure that the resulting bit length is
         --  smaller than the requested bit Size.

         Mpz_Clrbit (Rop       => Challenge,
                     Bit_Index => IC.unsigned_long (Rounds * 256 - 1));

         return Challenge;
      end;
   end Compute_Challenge;

   -------------------------------------------------------------------------

   function Compute_Commitment_Ak
     (Choice : Natural;
      Uk     : Uk_Array;
      Ek     : Mpz_T_Array;
      Zk     : Mpz_T_Array;
      N      : Mpz_T;
      N2     : Mpz_T)
      return Ak_Array
   is
      use type IC.int;

      Res : IC.int;
      Ak  : Ak_Array (Uk'Range);

      Zk_Pow_N, Uk_Pow_Ek : Mpz_T;
   begin
      if Choice not in Uk'Range then
         raise Calculation_Error with "Unable to calculate ak: invalid choice"
           & Choice'Img;
      end if;

      Mpz_Init (Integer => Zk_Pow_N);
      Mpz_Init (Integer => Uk_Pow_Ek);

      for K in Uk'Range loop
         Mpz_Init (Integer => Ak (K));

         --  zk^n mod n^2

         Mpz_Powm (Rop    => Zk_Pow_N,
                   Base   => Zk (K),
                   Exp    => N,
                   Modulo => N2);

         if K = Choice then

            --  k = i, ai := zi^n mod n^2

            Mpz_Set (Rop => Ak (K),
                     Op  => Zk_Pow_N);
         else

            --  k /= i, ak := zk^n*(uk^ek)^-1 mod n^2

            Mpz_Powm (Rop    => Uk_Pow_Ek,
                      Base   => Uk (K),
                      Exp    => Ek (K),
                      Modulo => N2);
            Mpz_Invert (Result => Res,
                        Rop    => Ak (K),
                        Op1    => Uk_Pow_Ek,
                        Op2    => N2);

            if Res = 0 then
               Mpz_Clear (Integer => Zk_Pow_N);
               Mpz_Clear (Integer => Uk_Pow_Ek);
               Mpz_Clear (Integers => Ak);
               raise Calculation_Error with "Unable to calculate ak" & K'Img
                 & ": uk^n has no inverse element in n^2 ="
                 & Mpz_Get_Ui (Op => N2)'Img;
            end if;

            Mpz_Mul (Rop => Ak (K),
                     Op1 => Ak (K),
                     Op2 => Zk_Pow_N);
            Mpz_Mod (R => Ak (K),
                     N => Ak (K),
                     D => N2);
         end if;

      end loop;

      Mpz_Clear (Integer => Zk_Pow_N);
      Mpz_Clear (Integer => Uk_Pow_Ek);

      return Ak;
   end Compute_Commitment_Ak;

   -------------------------------------------------------------------------

   procedure Compute_Hash
     (SHA_Context : in out GNAT.SHA256.Context;
      Election_Id :        String;
      Voter_Id    :        String;
      C           :        Mpz_T;
      Commitment  :        Ak_Array)
   is
      use GNAT;
   begin
      SHA256.Update (C     => SHA_Context,
                     Input => Election_Id);
      SHA256.Update (C     => SHA_Context,
                     Input => Voter_Id);

      SHA256.Update (C     => SHA_Context,
                     Input => Utils.To_Hex_String (Integer => C));

      for K in Commitment'Range loop
         SHA256.Update (C     => SHA_Context,
                        Input => Utils.To_Hex_String
                          (Integer => Commitment (K)));
      end loop;
   end Compute_Hash;

   -------------------------------------------------------------------------

   function Compute_Response_Ek
     (Choice    : Natural;
      Challenge : Mpz_T;
      Size      : Challenge_Index;
      Random_E  : Mpz_T_Array)
      return Ek_Array
   is
      Ek           : Ek_Array (Random_E'Range);
      Sum, B2, Two : Mpz_T;
   begin
      if Choice not in Ek'Range then
         raise Calculation_Error with "Unable to calculate ek: invalid choice"
           & Choice'Img;
      end if;

      Mpz_Init (Integer => B2);
      Mpz_Init_Set_Ui (Rop => Sum,
                       Op  => 0);
      Mpz_Init_Set_Ui (Rop => Two,
                       Op  => 2);

      for K in Ek'Range loop
         if K /= Choice then

            --  k /= i, ek := ek

            Mpz_Init_Set (Rop => Ek (K),
                          Op  => Random_E (K));
            Mpz_Add (Rop => Sum,
                     Op1 => Sum,
                     Op2 => Random_E (K));
         end if;
      end loop;

      --  k = i, ei := challenge - sum(ek, k /= i) mod 2^b

      Mpz_Pow_Ui (Rop  => B2,
                  Base => Two,
                  Exp  => IC.unsigned_long (Size));
      Mpz_Init (Integer => Ek (Choice));
      Mpz_Sub (Rop => Ek (Choice),
               Op1 => Challenge,
               Op2 => Sum);
      Mpz_Mod (R => Ek (Choice),
               N => Ek (Choice),
               D => B2);

      Mpz_Clear (Integer => Sum);
      Mpz_Clear (Integer => B2);
      Mpz_Clear (Integer => Two);

      return Ek;
   end Compute_Response_Ek;

   -------------------------------------------------------------------------

   function Compute_Response_Zk
     (Choice   : Natural;
      N        : Mpz_T;
      R        : Mpz_T;
      Random_Z : Mpz_T_Array;
      Ek       : Ek_Array)
      return Zk_Array
   is
      Zk   : Zk_Array (Ek'Range);
      R_Ei : Mpz_T;
   begin
      if Choice not in Zk'Range then
         raise Calculation_Error with "Unable to calculate zk: invalid choice"
           & Choice'Img;
      end if;

      for K in Zk'Range loop

         --  k /= i, zk := zk

         Mpz_Init_Set (Rop => Zk (K),
                       Op  => Random_Z (K));

         if K = Choice then

            --  k = i, zi := zi * r^ei mod n

            Mpz_Init (Integer => R_Ei);
            Mpz_Powm (Rop    => R_Ei,
                      Base   => R,
                      Exp    => Ek (K),
                      Modulo => N);
            Mpz_Mul (Rop => Zk (K),
                     Op1 => Zk (K),
                     Op2 => R_Ei);
            Mpz_Mod (R => Zk (K),
                     N => Zk (K),
                     D => N);
         end if;
      end loop;

      Mpz_Clear (Integer => R_Ei);

      return Zk;
   end Compute_Response_Zk;

   -------------------------------------------------------------------------

   function Compute_Uk
     (Mk : Mk_Array;
      G  : Mpz_T;
      N2 : Mpz_T;
      C  : Mpz_T)
      return Uk_Array
   is
      use type IC.int;

      Res            : IC.int;
      G_Inv, Gmk_Inv : Mpz_T;
      Uk             : Uk_Array (Mk'Range);
   begin
      Mpz_Init (Integer => G_Inv);
      Mpz_Init (Integer => Gmk_Inv);
      Mpz_Invert (Result => Res,
                  Rop    => G_Inv,
                  Op1    => G,
                  Op2    => N2);

      if Res = 0 then
         Mpz_Clear (Integer => G_Inv);
         Mpz_Clear (Integer => Gmk_Inv);
         raise Calculation_Error with "Unable to calculate uk: generator g ="
           & Mpz_Get_Ui (Op => G)'Img & " has no inverse element in n^2 ="
           & Mpz_Get_Ui (Op => N2)'Img;
      end if;

      for K in Uk'Range loop

         --  vk := g^mk mod n^2
         --  uk := c*vk^-1 mod n^2

         Mpz_Powm (Rop    => Gmk_Inv,
                   Base   => G_Inv,
                   Exp    => Mk (K),
                   Modulo => N2);

         Mpz_Init (Integer => Uk (K));
         Mpz_Mul (Rop => Uk (K),
                  Op1 => C,
                  Op2 => Gmk_Inv);
         Mpz_Mod (R => Uk (K),
                  N => Uk (K),
                  D => N2);
      end loop;

      Mpz_Clear (Integer => G_Inv);
      Mpz_Clear (Integer => Gmk_Inv);

      return Uk;
   end Compute_Uk;

   -------------------------------------------------------------------------

   function Create_Messages_Mk
     (N          : Mpz_T;
      Base       : Mpz_T;
      Candidates : Candidate_Count)
      return Mk_Array
   is
      Messages : Mk_Array (0 .. Candidates - 1);
   begin
      Mpz_Init_Set_Ui (Rop => Messages (0),
                       Op  => 0);

      for M in Positive range 1 .. Candidates - 1 loop
         Mpz_Init_Set (Rop => Messages (M),
                       Op  => Base);
         Mpz_Powm_Ui (Rop    => Messages (M),
                      Base   => Base,
                      Exp    => IC.unsigned_long (M - 1),
                      Modulo => N);
      end loop;

      return Messages;
   end Create_Messages_Mk;

   -------------------------------------------------------------------------

   function Encrypt
     (M  : Mpz_T;
      R  : Mpz_T;
      G  : Mpz_T;
      N  : Mpz_T;
      N2 : Mpz_T)
      return Mpz_T
   is
      C, G_M, R_N : Mpz_T;
   begin
      Mpz_Init (Integer => G_M);
      Mpz_Init (Integer => R_N);

      --  g^m mod n^2

      Mpz_Powm (Rop    => G_M,
                Base   => G,
                Exp    => M,
                Modulo => N2);

      --  r^n mod n^2

      Mpz_Powm (Rop    => R_N,
                Base   => R,
                Exp    => N,
                Modulo => N2);

      --  g^m*r^n mod n^2

      Mpz_Init (Integer => C);
      Mpz_Mul (Rop => C,
               Op1 => G_M,
               Op2 => R_N);
      Mpz_Mod (R => C,
               N => C,
               D => N2);

      Mpz_Clear (Integer => G_M);
      Mpz_Clear (Integer => R_N);

      return C;
   end Encrypt;

   -------------------------------------------------------------------------

   procedure Verify_Nth_Powers
     (Success : out Boolean;
      Err_Idx : out Integer;
      Uk      :     Uk_Array;
      Ak      :     Ak_Array;
      Ek      :     Ek_Array;
      Zk      :     Zk_Array;
      N       :     Mpz_T)
   is
      use type IC.int;

      Zk_N, Uk_Ek, N2, L_H : Mpz_T;

      procedure Cleanup;
      --  Free memory allocated by GMP bignums.

      procedure Cleanup
      is
      begin
         Mpz_Clear (Integer => N2);
         Mpz_Clear (Integer => Zk_N);
         Mpz_Clear (Integer => Uk_Ek);
         Mpz_Clear (Integer => L_H);
      end Cleanup;
   begin
      Success := True;
      Err_Idx := -1;

      Mpz_Init (Integer => N2);
      Mpz_Init (Integer => Zk_N);
      Mpz_Init (Integer => Uk_Ek);
      Mpz_Init (Integer => L_H);

      Mpz_Mul (Rop => N2,
               Op1 => N,
               Op2 => N);

      for K in Zk'Range loop

         --  zk^n mod n^2

         Mpz_Powm (Rop    => Zk_N,
                   Base   => Zk (K),
                   Exp    => N,
                   Modulo => N2);

         --  ak * uk^ek mod n^2

         Mpz_Powm (Rop    => Uk_Ek,
                   Base   => Uk (K),
                   Exp    => Ek (K),
                   Modulo => N2);
         Mpz_Mul (Rop => L_H,
                  Op1 => Ak (K),
                  Op2 => Uk_Ek);
         Mpz_Mod (R => L_H,
                  N => L_H,
                  D => N2);

         if Mpz_Cmp (Op1 => Zk_N,
                     Op2 => L_H) /= 0
         then
            Cleanup;
            Err_Idx := K;
            Success := False;
            return;
         end if;

      end loop;

      Cleanup;
   end Verify_Nth_Powers;

   -------------------------------------------------------------------------

   function Verify_Response_Ek
     (Challenge : Mpz_T;
      Ek        : Ek_Array)
      return Boolean
   is
      use type IC.int;

      Ek_Base : Mpz_T;
      Ek_Sum  : Mpz_T;
      Ch_Bits : IC.size_t;
   begin
      Mpz_Init_Set_Ui (Rop => Ek_Sum,
                       Op  => 0);

      for K in Ek'Range loop
         Mpz_Add (Rop => Ek_Sum,
                  Op1 => Ek_Sum,
                  Op2 => Ek (K));
      end loop;

      Ch_Bits := Mpz_Sizeinbase (Op   => Challenge,
                                 Base => 2);
      Mpz_Init (Integer => Ek_Base);
      Mpz_Ui_Pow_Ui (Rop  => Ek_Base,
                     Base => 2,
                     Exp  => IC.unsigned_long (Ch_Bits));
      Mpz_Mod (R => Ek_Sum,
               N => Ek_Sum,
               D => Ek_Base);

      return Result : Boolean do
         Result := Mpz_Cmp (Op1 => Ek_Sum,
                            Op2 => Challenge) = 0;

         Mpz_Clear (Integer => Ek_Sum);
         Mpz_Clear (Integer => Ek_Base);
      end return;
   end Verify_Response_Ek;

end ZKP.Paillier;
