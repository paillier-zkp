--
--  Copyright (C) 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--  University of Applied Sciences Rapperswil
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Strings.Unbounded;

package ZKP.Elections is

   use Ada.Strings.Unbounded;

   type Election_Type (<>) is private;
   --  Type aggregating all election-related data.

   type Candidates_Type is array (Natural range <>) of Unbounded_String;
   --  Array of candidates that can be voted for.

   type String_Array is array (Natural range <>) of Unbounded_String;
   --  Array of strings.

   function Create
     (ID          : String;
      Description : String;
      Candidates  : Candidates_Type;
      N           : String;
      G           : String;
      Exponent    : Positive)
      return Election_Type;
   --  Create an election instance with the specified parameters.

   function Get_ID (Election : Election_Type) return String;
   --  Returns the ID of the given election.

   function Get_Description (Election : Election_Type) return String;
   --  Returns the description of the given election.

   function Get_Candidates (Election : Election_Type) return Candidates_Type;
   --  Returns the candidates of the given election.

   function Get_N (Election : Election_Type) return String;
   --  Returns the modulus N of the given election as hex string.

   function Get_G (Election : Election_Type) return String;
   --  Returns the generator G of the given election as hex string.

   function Get_Exponent (Election : Election_Type) return Positive;
   --  Returns the base exponent of the given election.

   function Load_File (Filename : String) return Election_Type;
   --  Load election from specified file.

   function To_JSON_String (Election : Election_Type) return String;
   --  Returns the election as JSON string.

   type Ballot_Type (<>) is private;
   --  Ballot containing all information related to a cast vote.

   function Get_Election_ID (Ballot : Ballot_Type) return String;
   --  Returns the election ID of the given ballot.

   function Get_Voter_ID (Ballot : Ballot_Type) return String;
   --  Returns the voter ID of the given ballot.

   function Get_Vote (Ballot : Ballot_Type) return String;
   --  Returns the vote of the given ballot.

   function Get_Ak (Ballot : Ballot_Type) return String_Array;
   --  Returns the ak array of the given ballot.

   function Get_Ek (Ballot : Ballot_Type) return String_Array;
   --  Returns the ek array of the given ballot.

   function Get_Zk (Ballot : Ballot_Type) return String_Array;
   --  Returns the zk array of the given ballot.

   function Cast_Vote
     (Election : Election_Type;
      Voter_ID : String;
      Choice   : Natural;
      Cheat    : Boolean := False)
      return Ballot_Type;
      --  Cast vote of given choice for given election with specified voter id.
      --  If the cheat flag is True, the function creates an invalid ballot by
      --  voting two times for the selected choice.

   function Verify_Vote
     (Election : Election_Type;
      Ballot   : Ballot_Type)
      return Boolean;
   --  Returns True if the given ballot represents a correct vote in the
   --  context of the specified election.

   function Load_File (Filename : String) return Ballot_Type;
   --  Load ballot from specified file.

   function To_JSON_String (Ballot : Ballot_Type) return String;
   --  Returns the ballot as JSON string.

   Invalid_Election : exception;

private

   type Election_Type (Choices : Positive) is record
      ID          : Unbounded_String;
      Description : Unbounded_String;
      Candidates  : Candidates_Type (0 .. Choices);
      N           : Unbounded_String;
      G           : Unbounded_String;
      Exponent    : Positive;
   end record;

   type Ballot_Type (Choices : Positive) is record
      Election_ID : Unbounded_String;
      Voter_ID    : Unbounded_String;
      Vote        : Unbounded_String;
      Ak          : String_Array (0 .. Choices);
      Ek          : String_Array (0 .. Choices);
      Zk          : String_Array (0 .. Choices);
   end record;

end ZKP.Elections;
