--
--  Copyright (C) 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--  University of Applied Sciences Rapperswil
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

private with Ada.Strings.Unbounded;
private with Ada.Containers.Indefinite_Ordered_Maps;

package ZKP.Registry is

   type Shareholder_Registry_Type is private;
   --  Shareholder registry holding information about shareholders and their
   --  shares.

   function Size (Registry : Shareholder_Registry_Type) return Natural;
   --  Return number of registered shareholders.

   procedure Add_Shareholder
     (Registry : in out Shareholder_Registry_Type;
      Name     :        String;
      Shares   :        Long_Integer);
   --  Add shareholder with given name and specified amount of shares to
   --  registry.
   --  Raises shareholder present exception if a shareholder with the specified
   --  name is already registered.

   function Get_Share_Count
     (Registry : Shareholder_Registry_Type;
      Name     : String)
      return Long_Integer;
   --  Get number of shares for a given shareholder specified by name.
   --  Raises a shareholder not present exception if given shareholder name is
   --  not registered.

   function Contains
     (Registry : Shareholder_Registry_Type;
      Name     : String)
      return Boolean;
   --  Returns True if a shareholder entry for the specified name exists in the
   --  given registry.

   function Load_File (Filename : String) return Shareholder_Registry_Type;
   --  Load shareholder registry from specified file.

   Shareholder_Present     : exception;
   Shareholder_Not_Present : exception;

private

   use Ada.Strings.Unbounded;

   package SH_Data is new Ada.Containers.Indefinite_Ordered_Maps
     (Key_Type     => Unbounded_String,
      Element_Type => Long_Integer);

   type Shareholder_Registry_Type is record
      Data : SH_Data.Map;
   end record;

end ZKP.Registry;
