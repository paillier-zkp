--
--  Copyright (C) 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--  University of Applied Sciences Rapperswil
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

private with Ada.Containers.Doubly_Linked_Lists;
private with Ada.Strings.Unbounded;

package ZKP.Instructions is

   type Instruction_Type (<>) is private;
   --  Election voting instructions.

   function Get_Election_ID (Instruction : Instruction_Type) return String;
   --  Returns the election ID of the given voting instructions.

   function Get_Vote_Count (Instruction : Instruction_Type) return Natural;
   --  Returns the number of votes.

   procedure Iterate
     (Instruction : Instruction_Type;
      Process     : not null access
        procedure (Voter_ID : String;
                   Choice   : Natural;
                   Cheat    : Boolean));
   --  Iterate over all voting instructions and call the process procedure for
   --  each voter entry with the given parameters.

   function Load_File (Filename : String) return Instruction_Type;
   --  Load voting instructions from specified file.

private

   type Voter_Element_Type is record
      Voter_ID : Ada.Strings.Unbounded.Unbounded_String;
      Choice   : Natural;
      Cheat    : Boolean := False;
   end record;

   package LOIT is new Ada.Containers.Doubly_Linked_Lists
     (Element_Type => Voter_Element_Type);

   type Instruction_Type is record
      Election_ID : Ada.Strings.Unbounded.Unbounded_String;
      Voters      : LOIT.List;
   end record;

end ZKP.Instructions;
