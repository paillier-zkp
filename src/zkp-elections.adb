--
--  Copyright (C) 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--  University of Applied Sciences Rapperswil
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with GNATCOLL.JSON;

with Interfaces.C;

with ZKP.OS;
with ZKP.Random;
with ZKP.Paillier;
with ZKP.JSON;
with ZKP.Utils;
with ZKP.Logger;

package body ZKP.Elections is

   use ZKP.JSON;

   package L renames ZKP.Logger;

   function U
     (Item : String)
      return Ada.Strings.Unbounded.Unbounded_String
      renames Ada.Strings.Unbounded.To_Unbounded_String;

   procedure Calculate_Ballot
     (E        :     Election_Type;
      Voter_ID :     String;
      Choice   :     Natural;
      Cheat    :     Boolean := False;
      Vote     : out Unbounded_String;
      Ak_Str   : out String_Array;
      Ek_Str   : out String_Array;
      Zk_Str   : out String_Array);
   --  Calculate ballot data (vote) and corresponding zero-knowledge proof
   --  information Ak, Ek and Zk for given voter and choice in election E. If
   --  the cheat flag is True, the function creates an invalid vote by voting
   --  two times for the selected choice.

   procedure JSON_To_Ballot
     (Ballot : in out Ballot_Type;
      Value  :        GNATCOLL.JSON.JSON_Value);
   --  Assign values extracted from given JSON value to ballot instance.

   procedure JSON_To_Election
     (Election : in out Election_Type;
      Value    :        GNATCOLL.JSON.JSON_Value);
   --  Assign values extracted from given JSON value to election instance.

   -------------------------------------------------------------------------

   procedure Calculate_Ballot
     (E        :     Election_Type;
      Voter_ID :     String;
      Choice   :     Natural;
      Cheat    :     Boolean := False;
      Vote     : out Unbounded_String;
      Ak_Str   : out String_Array;
      Ek_Str   : out String_Array;
      Zk_Str   : out String_Array)
   is
      use GMP.Binding;

      subtype Choices_Range is Natural range 0 .. E.Choices;

      Mk                    : Paillier.Mk_Array (Choices_Range);
      N_Bits                : Positive;
      Ex, C, R, G, N, N2, M : Mpz_T;
   begin
      N := Utils.To_Bignum (Hex_Str => Get_N (Election => E));
      G := Utils.To_Bignum (Hex_Str => Get_G (Election => E));

      N_Bits := Positive (Mpz_Sizeinbase (Op   => N,
                                          Base => 2));
      pragma Debug (L.Log ("n is" & N_Bits'Img & " bits"));

      Mpz_Init_Set_Ui (Rop => Ex,
                       Op  => 2);
      Mpz_Pow_Ui (Rop  => Ex,
                  Base => Ex,
                  Exp  => Interfaces.C.unsigned_long (E.Exponent));

      Mk := Paillier.Create_Messages_Mk
        (N          => N,
         Base       => Ex,
         Candidates => E.Choices + 1);
      Mpz_Clear (Integer => Ex);
      pragma Debug (L.Log (Utils.To_String
        (Name    => "m",
         Numbers => Mpz_T_Array (Mk))));

      R := Random.Get_Coprime (Number => N);
      pragma Debug (L.Log ("r " & Utils.To_Hex_String (Integer => R)));

      Mpz_Init (Integer => N2);
      Mpz_Mul (Rop => N2,
               Op1 => N,
               Op2 => N);

      Mpz_Init_Set (Rop => M,
                    Op  => Mk (Choice));
      if Cheat then
         pragma Debug (L.Log (">>> CHEATING ENABLED <<<"));
         Mpz_Mul_Ui (Rop => M,
                     Op1 => M,
                     Op2 => 2);
      end if;

      C := Paillier.Encrypt
        (M  => M,
         R  => R,
         G  => G,
         N  => N,
         N2 => N2);
      Mpz_Clear (Integer => M);
      Vote := U (Utils.To_Hex_String (Integer => C));
      pragma Debug (L.Log ("c " & Utils.To_Hex_String (Integer => C)));

      declare
         Random_E, Random_Z : Mpz_T_Array (Choices_Range);
         Uk : Paillier.Uk_Array (Choices_Range);
         Ak : Paillier.Ak_Array (Choices_Range);
         Ek : Paillier.Ek_Array (Choices_Range);
         Zk : Paillier.Zk_Array (Choices_Range);
         Challenge : Mpz_T;
      begin
         Random_E := Random.Get
           (Bit_Size => (N_Bits / 2) - 1,
            Count    => E.Choices + 1);
         pragma Debug (L.Log (Utils.To_String
           (Name    => "random_e",
            Numbers => Random_E)));

         Random_Z := Random.Get_Coprimes
           (Number => N,
            Count  => E.Choices + 1);
         pragma Debug (L.Log (Utils.To_String
           (Name    => "random_z",
            Numbers => Random_Z)));

         Uk := Paillier.Compute_Uk
           (Mk => Mk,
            G  => G,
            N2 => N2,
            C  => C);
         pragma Debug (L.Log (Utils.To_String
           (Name    => "u",
            Numbers => Mpz_T_Array (Uk))));

         Ak := Paillier.Compute_Commitment_Ak
           (Choice => Choice,
            Uk     => Uk,
            Ek     => Random_E,
            Zk     => Random_Z,
            N      => N,
            N2     => N2);

         Challenge := Paillier.Compute_Challenge
           (Election_Id => Get_ID (Election => E),
            Voter_Id    => Voter_ID,
            C           => C,
            Commitment  => Ak,
            Size        => N_Bits / 2);
         pragma Debug (L.Log ("challenge " & Utils.To_Hex_String
           (Integer => Challenge)));

         Ek := Paillier.Compute_Response_Ek
           (Choice    => Choice,
            Challenge => Challenge,
            Size      => (N_Bits / 2) - 1,
            Random_E  => Random_E);
         Zk := Paillier.Compute_Response_Zk
           (Choice   => Choice,
            N        => N,
            R        => R,
            Random_Z => Random_Z,
            Ek       => Ek);

         for I in Choices_Range loop
            Ak_Str (I) := U (Utils.To_Hex_String (Integer => Ak (I)));
            Ek_Str (I) := U (Utils.To_Hex_String (Integer => Ek (I)));
            Zk_Str (I) := U (Utils.To_Hex_String (Integer => Zk (I)));
         end loop;

         Mpz_Clear (Integer => Challenge);
         Mpz_Clear (Integers => Mpz_T_Array (Random_E));
         Mpz_Clear (Integers => Mpz_T_Array (Random_Z));
         Mpz_Clear (Integers => Mpz_T_Array (Ak));
         Mpz_Clear (Integers => Mpz_T_Array (Ek));
         Mpz_Clear (Integers => Mpz_T_Array (Uk));
         Mpz_Clear (Integers => Mpz_T_Array (Zk));
      end;

      Mpz_Clear (Integers => Mpz_T_Array (Mk));
      Mpz_Clear (Integer => C);
      Mpz_Clear (Integer => N);
      Mpz_Clear (Integer => N2);
      Mpz_Clear (Integer => G);
      Mpz_Clear (Integer => R);
   end Calculate_Ballot;

   -------------------------------------------------------------------------

   function Cast_Vote
     (Election : Election_Type;
      Voter_ID : String;
      Choice   : Natural;
      Cheat    : Boolean := False)
      return Ballot_Type
   is
   begin
      if not (Choice <= Election.Choices) then
         raise Invalid_Election with "Choice" & Choice'Img & " is invalid: " &
         "must be in range 1 .." & Election.Candidates'Last'Img;
      end if;

      pragma Debug (L.Log ("--  Cast vote ---------------------------------"));
      pragma Debug (L.Log (Utils.To_String
        (E      => Election,
         Voter  => Voter_ID)));
      pragma Debug (L.Log (Utils.To_String
        (Candidates => Get_Candidates (Election),
         Choice     => Choice)));

      return B : Ballot_Type (Choices => Election.Choices) do
         B.Election_ID := Election.ID;
         B.Voter_ID    := U (Voter_ID);

         Calculate_Ballot
           (E        => Election,
            Voter_ID => Voter_ID,
            Choice   => Choice,
            Cheat    => Cheat,
            Vote     => B.Vote,
            Ak_Str   => B.Ak,
            Ek_Str   => B.Ek,
            Zk_Str   => B.Zk);
      end return;
   end Cast_Vote;

   -------------------------------------------------------------------------

   function Create
     (ID          : String;
      Description : String;
      Candidates  : Candidates_Type;
      N           : String;
      G           : String;
      Exponent    : Positive)
      return Election_Type
   is
   begin
      return E : Election_Type (Choices => Candidates'Length - 1) do
         E.ID          := U (ID);
         E.Description := U (Description);
         E.Candidates  := Candidates;
         E.N           := U (N);
         E.G           := U (G);
         E.Exponent    := Exponent;
      end return;
   end Create;

   -------------------------------------------------------------------------

   function Get_Ak (Ballot : Ballot_Type) return String_Array
   is
   begin
      return Ballot.Ak;
   end Get_Ak;

   -------------------------------------------------------------------------

   function Get_Candidates (Election : Election_Type) return Candidates_Type
   is
   begin
      return Election.Candidates;
   end Get_Candidates;

   -------------------------------------------------------------------------

   function Get_Description (Election : Election_Type) return String
   is
   begin
      return To_String (Source => Election.Description);
   end Get_Description;

   -------------------------------------------------------------------------

   function Get_Ek (Ballot : Ballot_Type) return String_Array
   is
   begin
      return Ballot.Ek;
   end Get_Ek;

   -------------------------------------------------------------------------

   function Get_Election_ID (Ballot : Ballot_Type) return String
   is
   begin
      return To_String (Ballot.Election_ID);
   end Get_Election_ID;

   -------------------------------------------------------------------------

   function Get_Exponent (Election : Election_Type) return Positive
   is
   begin
      return Election.Exponent;
   end Get_Exponent;

   -------------------------------------------------------------------------

   function Get_G (Election : Election_Type) return String
   is
   begin
      return To_String (Election.G);
   end Get_G;

   -------------------------------------------------------------------------

   function Get_ID (Election : Election_Type) return String
   is
   begin
      return To_String (Source => Election.ID);
   end Get_ID;

   -------------------------------------------------------------------------

   function Get_N (Election : Election_Type) return String
   is
   begin
      return To_String (Election.N);
   end Get_N;

   -------------------------------------------------------------------------

   function Get_Vote (Ballot : Ballot_Type) return String
   is
   begin
      return To_String (Ballot.Vote);
   end Get_Vote;

   -------------------------------------------------------------------------

   function Get_Voter_ID (Ballot : Ballot_Type) return String
   is
   begin
      return To_String (Ballot.Voter_ID);
   end Get_Voter_ID;

   -------------------------------------------------------------------------

   function Get_Zk (Ballot : Ballot_Type) return String_Array
   is
   begin
      return Ballot.Zk;
   end Get_Zk;

   -------------------------------------------------------------------------

   procedure JSON_To_Ballot
     (Ballot : in out Ballot_Type;
      Value  :        GNATCOLL.JSON.JSON_Value)
   is
      use GNATCOLL.JSON;

      Proof : constant JSON_Array := Value.Get (Field => Proof_Fieldname);
   begin
      Ballot.Election_ID := Value.Get (Field => E_ID_Fieldname);
      Ballot.Voter_ID    := Value.Get (Field => Voter_ID_Fieldname);
      Ballot.Vote        := Value.Get (Field => Ballot_Fieldname);

      for P in 0 .. Length (Proof) - 1 loop
         declare
            Val :  constant JSON_Value
              := Get (Arr   => Proof,
                      Index => P + 1);
         begin
            Ballot.Ak (P) := Get (Val   => Val,
                                  Field => A_Fieldname);
            Ballot.Ek (P) := Get (Val   => Val,
                                  Field => E_Fieldname);
            Ballot.Zk (P) := Get (Val   => Val,
                                  Field => Z_Fieldname);
         end;
      end loop;
   end JSON_To_Ballot;

   -------------------------------------------------------------------------

   procedure JSON_To_Election
     (Election : in out Election_Type;
      Value    :        GNATCOLL.JSON.JSON_Value)
   is
      use GNATCOLL.JSON;

      Candidates : constant JSON_Array := Value.Get
        (Field => Candidates_Fieldname);
   begin
      Election.ID          := Value.Get (Field => E_ID_Fieldname);
      Election.Description := Value.Get (Field => Desc_Fieldname);

      for C in 0 .. Length (Candidates) - 1 loop
         Election.Candidates (C)
           := Get (Val => Get
                   (Arr   => Candidates,
                    Index => C + 1));
      end loop;

      Election.N := U (Value.Get (Field => N_Fieldname));
      Election.G := U (Value.Get (Field => G_Fieldname));

      Election.Exponent := Value.Get (Field => Exponent_Fieldname);
   end JSON_To_Election;

   -------------------------------------------------------------------------

   function Load_File (Filename : String) return Election_Type
   is
      use GNATCOLL.JSON;

      Content    : constant String     := OS.Read_File (Filename => Filename);
      Val        : constant JSON_Value := GNATCOLL.JSON.Read
        (Strm     => Content,
         Filename => Filename);
      Candidates : constant JSON_Array := Val.Get
        (Field => Candidates_Fieldname);
   begin
      return E : Election_Type (Choices => Length (Candidates) - 1) do
         JSON_To_Election (Election => E,
                           Value    => Val);
      end return;
   end Load_File;

   -------------------------------------------------------------------------

   function Load_File (Filename : String) return Ballot_Type
   is
      use GNATCOLL.JSON;

      Content : constant String     := OS.Read_File (Filename => Filename);
      Val     : constant JSON_Value := GNATCOLL.JSON.Read
        (Strm     => Content,
         Filename => Filename);
      Proof   : constant JSON_Array := Val.Get
        (Field => Proof_Fieldname);
   begin
      return B : Ballot_Type (Choices => Length (Proof) - 1) do
         JSON_To_Ballot (Ballot => B,
                         Value  => Val);
      end return;
   end Load_File;

   -------------------------------------------------------------------------

   function To_JSON_String (Ballot : Ballot_Type) return String
   is
      use GNATCOLL.JSON;

      B_Json : constant JSON_Value := Create_Object;
   begin
      B_Json.Set_Field (Field_Name => E_ID_Fieldname,
                        Field      => Ballot.Election_ID);
      B_Json.Set_Field (Field_Name => Voter_ID_Fieldname,
                        Field      => Ballot.Voter_ID);
      B_Json.Set_Field (Field_Name => Ballot_Fieldname,
                        Field      => Ballot.Vote);

      declare
         Proof_Json : JSON_Array;
      begin
         for I in Ballot.Ak'Range loop
            declare
               Proof_Part : constant JSON_Value := Create_Object;
            begin
               Proof_Part.Set_Field
                 (Field_Name => A_Fieldname,
                  Field      => Ballot.Ak (I));
               Proof_Part.Set_Field
                 (Field_Name => E_Fieldname,
                  Field      => Ballot.Ek (I));
               Proof_Part.Set_Field
                 (Field_Name => Z_Fieldname,
                  Field      => Ballot.Zk (I));
               Append (Arr => Proof_Json,
                       Val => Proof_Part);
            end;
         end loop;
         B_Json.Set_Field (Field_Name => Proof_Fieldname,
                           Field      => Proof_Json);
      end;

      return B_Json.Write;
   end To_JSON_String;

   -------------------------------------------------------------------------

   function To_JSON_String (Election : Election_Type) return String
   is
      use GNATCOLL.JSON;

      E_Json : constant JSON_Value := Create_Object;
   begin
      E_Json.Set_Field (Field_Name => E_ID_Fieldname,
                        Field      => Election.ID);
      E_Json.Set_Field (Field_Name => Desc_Fieldname,
                        Field      => Election.Description);

      declare
         Candidates_Json : JSON_Array;
      begin
         for C in Election.Candidates'Range loop
            Append (Arr => Candidates_Json,
                    Val => Create (Val => Election.Candidates (C)));
         end loop;
         E_Json.Set_Field (Field_Name => Candidates_Fieldname,
                           Field      => Candidates_Json);
      end;

      E_Json.Set_Field
        (Field_Name => N_Fieldname,
         Field      => Election.N);
      E_Json.Set_Field
        (Field_Name => G_Fieldname,
         Field      => Election.G);
      E_Json.Set_Field
        (Field_Name => Exponent_Fieldname,
         Field      => Election.Exponent);

      return E_Json.Write;
   end To_JSON_String;

   -------------------------------------------------------------------------

   function Verify_Vote
     (Election : Election_Type;
      Ballot   : Ballot_Type)
      return Boolean
   is
      use GMP.Binding;

      subtype Choices_Range is Natural range 0 .. Election.Choices;

      N_Bits          : Positive;

      Ex, C, N, N2, G : Mpz_T;
      Challenge       : Mpz_T;

      Mk              : Paillier.Mk_Array (Choices_Range);
      Uk              : Paillier.Uk_Array (Choices_Range);
      Commitment      : Paillier.Ak_Array (Choices_Range);
      Response_Ek     : Paillier.Ek_Array (Choices_Range);
      Response_Zk     : Paillier.Zk_Array (Choices_Range);

      procedure Cleanup;
      --  Free memory related to Mpz_T variables and arrays.

      procedure Cleanup
      is
      begin
         Mpz_Clear (Integer => C);
         Mpz_Clear (Integer => N);
         Mpz_Clear (Integer => N2);
         Mpz_Clear (Integer => G);
         Mpz_Clear (Integer => Challenge);
         Mpz_Clear (Integers => Mpz_T_Array (Mk));
         Mpz_Clear (Integers => Mpz_T_Array (Uk));
         Mpz_Clear (Integers => Mpz_T_Array (Commitment));
         Mpz_Clear (Integers => Mpz_T_Array (Response_Ek));
         Mpz_Clear (Integers => Mpz_T_Array (Response_Zk));
      end Cleanup;
   begin
      pragma Debug (L.Log ("--  Verify vote -------------------------------"));
      pragma Debug (L.Log (Utils.To_String
        (E      => Election,
         Voter  => Get_Voter_ID (Ballot))));

      G      := Utils.To_Bignum (Hex_Str => Get_G (Election => Election));
      N      := Utils.To_Bignum (Hex_Str => Get_N (Election => Election));
      N_Bits := Positive (Mpz_Sizeinbase (Op   => N,
                                          Base => 2));
      pragma Debug (L.Log ("n is" & N_Bits'Img & " bits"));

      C := Utils.To_Bignum (Hex_Str => Get_Vote (Ballot => Ballot));
      pragma Debug (L.Log ("c " & Utils.To_Hex_String (Integer => C)));

      Mpz_Init (Integer => N2);
      Mpz_Mul (Rop => N2,
               Op1 => N,
               Op2 => N);

      Mpz_Init_Set_Ui (Rop => Ex,
                       Op  => 2);
      Mpz_Pow_Ui (Rop  => Ex,
                  Base => Ex,
                  Exp  => Interfaces.C.unsigned_long (Election.Exponent));

      Mk := Paillier.Create_Messages_Mk
        (N          => N,
         Base       => Ex,
         Candidates => Election.Choices + 1);
      Mpz_Clear (Integer => Ex);
      pragma Debug (L.Log (Utils.To_String
        (Name    => "m",
         Numbers => Mpz_T_Array (Mk))));

      Uk := Paillier.Compute_Uk
        (Mk => Mk,
         G  => G,
         N2 => N2,
         C  => C);
      pragma Debug (L.Log (Utils.To_String
        (Name    => "u",
         Numbers => Mpz_T_Array (Uk))));

      for I in Ballot.Ak'Range loop
         Commitment (I)  := Utils.To_Bignum
           (Hex_Str => To_String (Ballot.Ak (I)));
         Response_Ek (I) := Utils.To_Bignum
           (Hex_Str => To_String (Ballot.Ek (I)));
         Response_Zk (I) := Utils.To_Bignum
           (Hex_Str => To_String (Ballot.Zk (I)));
      end loop;

      Challenge := Paillier.Compute_Challenge
        (Election_Id => Get_ID (Election => Election),
         Voter_Id    => Get_Voter_ID (Ballot => Ballot),
         C           => C,
         Commitment  => Commitment,
         Size        => N_Bits / 2);
      pragma Debug (L.Log ("challenge " & Utils.To_Hex_String
        (Integer => Challenge)));

      if not Paillier.Verify_Response_Ek
        (Challenge => Challenge,
         Ek        => Response_Ek)
      then
         pragma Debug (L.Log ("!!! Response Ek not valid"));
         Cleanup;

         return False;
      end if;

      declare
         Index  : Integer;
         Result : Boolean;
      begin
         Paillier.Verify_Nth_Powers
           (Success => Result,
            Err_Idx => Index,
            Uk      => Uk,
            Ak      => Commitment,
            Ek      => Response_Ek,
            Zk      => Response_Zk,
            N       => N);
         Cleanup;

         if not Result then
            pragma Debug
              (L.Log ("!!! Nth powers not valid (" & Index'Img & " )"));
            null;
         end if;

         return Result;
      end;
   end Verify_Vote;

end ZKP.Elections;
