--
--  Copyright (C) 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--  University of Applied Sciences Rapperswil
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

package ZKP.Random is

   use GMP.Binding;

   function Get (Bit_Size : Natural) return Mpz_T;
   --  Return a uniformly distributed random integer in the range 0 to
   --  2^Bit_Size-1, inclusive.

   function Get
     (Bit_Size : Natural;
      Count    : Positive)
      return Mpz_T_Array;
   --  Return Count number of uniformly distributed random integers in the
   --  range 0 to 2^Bit_Size-1, inclusive.

   function Get_Coprime (Number : Mpz_T) return Mpz_T;
   --  Return a random integer coprime to the specified Number, i.e. the
   --  generated random number is from the set of integers coprime to n
   --  (Zn*) - this set consists of Phi(n) number of integers.

   function Get_Coprimes
     (Number : Mpz_T;
      Count  : Positive)
      return Mpz_T_Array;
   --  Return Count number of random integers coprime to the specified number.

   Seed_Error : exception;

end ZKP.Random;
