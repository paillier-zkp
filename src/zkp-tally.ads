--
--  Copyright (C) 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--  University of Applied Sciences Rapperswil
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

private with Ada.Strings.Unbounded.Hash;
private with Ada.Finalization;
private with Ada.Containers.Indefinite_Hashed_Sets;

private with GMP.Binding;

with ZKP.Elections;
with ZKP.Registry;

package ZKP.Tally is

   type Tally_Type (<>) is limited private;
   --  Election tally.

   function Create_Tally
     (Election    : Elections.Election_Type;
      SH_Registry : Registry.Shareholder_Registry_Type)
      return Tally_Type;
   --  Create and initialize tally for specified election and shareholder
   --  registry.

   function Get_Election_ID (Tally : Tally_Type) return String;
   --  Returns the election ID of the given tally.

   function Get_Value (Tally : Tally_Type) return String;
   --  Returns the encrypted tally value as hex string.

   procedure Add_Ballot
     (Tally  : in out Tally_Type;
      Ballot :        Elections.Ballot_Type);
   --  Adds the given ballot to the tally. The vote in the ballot is weighted
   --  with the share count for the corresponding voter id.

   function To_JSON_String (Tally : Tally_Type) return String;
   --  Returns the tally as JSON string.

   Duplicate_Voter : exception;

private

   use Ada.Strings.Unbounded;

   package VIHS is new Ada.Containers.Indefinite_Hashed_Sets
     (Element_Type        => Unbounded_String,
      Hash                => Hash,
      Equivalent_Elements => "=");

   type Tally_Type is new Ada.Finalization.Limited_Controlled with record
      Election_ID : Ada.Strings.Unbounded.Unbounded_String;
      SH_Registry : Registry.Shareholder_Registry_Type;
      Value       : GMP.Binding.Mpz_T;
      N2          : GMP.Binding.Mpz_T;
      Voters      : VIHS.Set;
      Initialized : Boolean := False;
   end record;

   overriding
   procedure Finalize (Object : in out Tally_Type);
   --  Clear all Mpz_T record fields.

end ZKP.Tally;
