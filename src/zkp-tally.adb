--
--  Copyright (C) 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--  University of Applied Sciences Rapperswil
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Interfaces.C;

with GNATCOLL.JSON;

with ZKP.JSON;
with ZKP.Utils;

package body ZKP.Tally is

   package IC renames Interfaces.C;

   function U (S : String) return Ada.Strings.Unbounded.Unbounded_String
               renames Ada.Strings.Unbounded.To_Unbounded_String;

   -------------------------------------------------------------------------

   procedure Add_Ballot
     (Tally  : in out Tally_Type;
      Ballot :        Elections.Ballot_Type)
   is
      use GMP.Binding;

      V_ID   : constant String := Elections.Get_Voter_ID (Ballot => Ballot);
      Weight : constant Long_Integer
        := Registry.Get_Share_Count
          (Registry => Tally.SH_Registry,
           Name     => V_ID);
      W      : Mpz_T;
      C      : Mpz_T := Utils.To_Bignum
        (Hex_Str => Elections.Get_Vote (Ballot => Ballot));

      procedure Cleanup;
      --  Clear all initialized mpz variables.

      procedure Cleanup
      is
      begin
         Mpz_Clear (Integer => C);
         Mpz_Clear (Integer => W);
      end Cleanup;
   begin

      --  Calculate weighted vote: C = C ^ W mod n^2

      Mpz_Init_Set_Ui (Rop => W,
                       Op  => IC.unsigned_long (Weight));

      Mpz_Powm (Rop    => C,
                Base   => C,
                Exp    => W,
                Modulo => Tally.N2);

      if Tally.Voters.Contains (Item => U (V_ID)) then
         raise Duplicate_Voter with "Vote for voter '" & V_ID
           & "' already added to tally";
      end if;

      if not Tally.Initialized then

         --  Set first vote as tally, nothing else to do.

         Mpz_Set (Rop => Tally.Value,
                  Op  => C);
         Tally.Voters.Insert (New_Item => U (V_ID));
         Tally.Initialized := True;

         Cleanup;
         return;
      end if;

      --  Add vote to tally: T = T * C mod n^2

      Mpz_Mul (Rop => Tally.Value,
               Op1 => Tally.Value,
               Op2 => C);
      Mpz_Mod (R => Tally.Value,
               N => Tally.Value,
               D => Tally.N2);
      Tally.Voters.Insert (New_Item => U (V_ID));

      Cleanup;

   exception
      when others =>
         Cleanup;
         raise;
   end Add_Ballot;

   -------------------------------------------------------------------------

   function Create_Tally
     (Election    : Elections.Election_Type;
      SH_Registry : Registry.Shareholder_Registry_Type)
      return Tally_Type
   is
      use GMP.Binding;

      N : Mpz_T := Utils.To_Bignum
        (Hex_Str => Elections.Get_N (Election => Election));
   begin
      return T : Tally_Type do
         T.Election_ID := U (Elections.Get_ID (Election => Election));
         T.SH_Registry := SH_Registry;
         Mpz_Init (Integer => T.Value);

         Mpz_Init (Integer => T.N2);
         Mpz_Mul (Rop => T.N2,
                  Op1 => N,
                  Op2 => N);
         Mpz_Clear (Integer => N);
      end return;
   end Create_Tally;

   -------------------------------------------------------------------------

   procedure Finalize (Object : in out Tally_Type)
   is
   begin
      GMP.Binding.Mpz_Clear (Integer => Object.Value);
      GMP.Binding.Mpz_Clear (Integer => Object.N2);
   end Finalize;

   -------------------------------------------------------------------------

   function Get_Election_ID (Tally : Tally_Type) return String
   is
   begin
      return Ada.Strings.Unbounded.To_String (Tally.Election_ID);
   end Get_Election_ID;

   -------------------------------------------------------------------------

   function Get_Value (Tally : Tally_Type) return String
   is
   begin
      return Utils.To_Hex_String (Integer => Tally.Value);
   end Get_Value;

   -------------------------------------------------------------------------

   function To_JSON_String (Tally : Tally_Type) return String
   is
      use GNATCOLL.JSON;

      T_Json : constant JSON_Value := Create_Object;
   begin
      T_Json.Set_Field (Field_Name => ZKP.JSON.E_ID_Fieldname,
                        Field      => Tally.Election_ID);
      T_Json.Set_Field (Field_Name => ZKP.JSON.Tally_Fieldname,
                        Field      => Utils.To_Hex_String
                          (Integer => Tally.Value));

      return T_Json.Write;
   end To_JSON_String;

end ZKP.Tally;
