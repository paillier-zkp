--
--  Copyright (C) 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--  University of Applied Sciences Rapperswil
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with GNATCOLL.JSON;

with ZKP.OS;
with ZKP.JSON;

package body ZKP.Instructions is

   -------------------------------------------------------------------------

   function Get_Election_ID (Instruction : Instruction_Type) return String
   is
   begin
      return Ada.Strings.Unbounded.To_String
        (Source => Instruction.Election_ID);
   end Get_Election_ID;

   -------------------------------------------------------------------------

   function Get_Vote_Count (Instruction : Instruction_Type) return Natural
   is
   begin
      return Natural (Instruction.Voters.Length);
   end Get_Vote_Count;

   -------------------------------------------------------------------------

   procedure Iterate
     (Instruction : Instruction_Type;
      Process     : not null access
        procedure (Voter_ID : String;
                   Choice   : Natural;
                   Cheat    : Boolean))
   is
      procedure Call_Process (Pos : LOIT.Cursor);
      --  Call process for list element at given position.

      procedure Call_Process (Pos : LOIT.Cursor)
      is
         use Ada.Strings.Unbounded;

         Element : constant Voter_Element_Type
           := LOIT.Element (Position => Pos);
      begin
         Process (Voter_ID => To_String (Element.Voter_ID),
                  Choice   => Element.Choice,
                  Cheat    => Element.Cheat);
      end Call_Process;
   begin
      Instruction.Voters.Iterate (Process => Call_Process'Access);
   end Iterate;

   -------------------------------------------------------------------------

   function Load_File (Filename : String) return Instruction_Type
   is
      use GNATCOLL.JSON;
      use ZKP.JSON;

      Content : constant String     := OS.Read_File (Filename => Filename);
      Value   : constant JSON_Value
        := Read (Strm     => Content,
                 Filename => Filename);
      Voters  : constant JSON_Array := Value.Get (Field => Voters_Fieldname);
   begin
      return Instruction : Instruction_Type do
         Instruction.Election_ID := Value.Get (Field => E_ID_Fieldname);

         for I in 1 .. Length (Voters) loop
            declare
               Val : constant JSON_Value
                 := Get (Arr   => Voters,
                         Index => I);
               Vote : Voter_Element_Type;
            begin
               Vote.Voter_ID := Get (Val   => Val,
                                     Field => Voter_ID_Fieldname);
               Vote.Choice   := Get (Val   => Val,
                                     Field => Vote_Fieldname);
               Vote.Cheat    := Get (Val   => Val,
                                     Field => Cheat_Fieldname);

               Instruction.Voters.Append (New_Item => Vote);
            end;
         end loop;
      end return;
   end Load_File;

end ZKP.Instructions;
