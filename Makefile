TESTDIR = tests
OBJDIR  = obj
COVDIR  = $(OBJDIR)/cov

GMAKE_OPTS = -p -R

all: build_tools

build_tests:
	@gnatmake $(GMAKE_OPTS) -Ppaillier_zkp_tests

build_tools:
	@gnatmake $(GMAKE_OPTS) -Ppaillier_zkp

build_all: build_tests build_tools

tests: build_tests
	@$(OBJDIR)/$(TESTDIR)/test_runner

cov:
	@rm -f $(COVDIR)/*.gcda
	@gnatmake $(GMAKE_OPTS) -Ppaillier_zkp_tests -XBUILD="coverage"
	@$(COVDIR)/test_runner || true
	@lcov -c -d $(COVDIR) -o $(COVDIR)/cov.info
	@lcov -e $(COVDIR)/cov.info "$(PWD)/src/*.adb" -o $(COVDIR)/cov.info
	@genhtml --no-branch-coverage $(COVDIR)/cov.info -o $(COVDIR)

doc:
	@$(MAKE) -C doc

clean:
	@rm -rf $(OBJDIR)
	@$(MAKE) -C doc clean

.PHONY: build_all build_tests build_tools clean cov doc tests
