--
--  Copyright (C) 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--  University of Applied Sciences Rapperswil
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Command_Line;
with Ada.Exceptions;
with Ada.Strings.Fixed;
with Ada.Text_IO;

with ZKP.Elections;
with ZKP.Registry;
with ZKP.Tally;
with ZKP.OS;

procedure Tally
is
   use ZKP.Elections;
   use ZKP.Registry;
   use ZKP.Tally;

   Output_Filename   : constant String := "obj/encrypted_tally.json";
   Ballot_Prefix     : constant String := "obj/ballot-";
   Election_Filename : constant String := "data/public_key.json";
   Registry_Filename : constant String := "data/novartis_registry.json";

   Election          : constant Election_Type
     := Load_File (Filename => Election_Filename);
   SH_Registry       : constant Shareholder_Registry_Type
     := Load_File (Filename => Registry_Filename);
   Weighted_Tally    : Tally_Type := Create_Tally
     (Election    => Election,
      SH_Registry => SH_Registry);
begin
   Ada.Text_IO.Put_Line
     ("Computing tally for election '" & Get_ID (Election => Election) & "'");
   Ada.Text_IO.New_Line;

   for I in Positive range 1 .. Size (Registry => SH_Registry) loop
      declare
         B_Filename : constant String
           := Ballot_Prefix
             & Ada.Strings.Fixed.Trim (Source => I'Img,
                                       Side   => Ada.Strings.Left) & ".json";
         B          : constant Ballot_Type
           := Load_File (Filename => B_Filename);
      begin
         if not Contains
           (Registry => SH_Registry,
            Name     => Get_Voter_ID (Ballot => B))
         then
            Ada.Text_IO.Put_Line
              ("Status      : Skipped (no registry entry)");
         elsif Get_Election_ID
           (Ballot => B) /=  Get_Election_ID (Tally => Weighted_Tally)
         then
            Ada.Text_IO.Put_Line
              ("Status      : Skipped (election id mismatch ('"
               & Get_Election_ID (Ballot => B)
               & "' but expected '" & Get_ID (Election => Election) & "')");
         elsif not Verify_Vote
           (Election => Election,
            Ballot   => B)
         then
            Ada.Text_IO.Put_Line ("Status      : Skipped (invalid vote)");
         else
            Add_Ballot (Tally  => Weighted_Tally,
                        Ballot => B);

            Ada.Text_IO.Put_Line ("Shares      :"
              & Get_Share_Count
                (Registry => SH_Registry,
                 Name     => Get_Voter_ID (Ballot => B))'Img);
            Ada.Text_IO.Put_Line ("Status      : Counted");
         end if;
      end;
   end loop;

   ZKP.OS.Write_File (Content  => To_JSON_String (Tally => Weighted_Tally),
                      Filename => Output_Filename);

   Ada.Text_IO.New_Line;
   Ada.Text_IO.Put_Line
     ("Stored tally for election '" & Get_ID (Election => Election)
      & "' to '" & Output_Filename & "'");

   Ada.Command_Line.Set_Exit_Status (Code => Ada.Command_Line.Success);

exception
   when E : others =>
      Ada.Text_IO.Put_Line ("Processing error:");
      Ada.Text_IO.Put_Line (Ada.Exceptions.Exception_Information (X => E));
      Ada.Command_Line.Set_Exit_Status (Code => Ada.Command_Line.Failure);
end Tally;
