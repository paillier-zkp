--
--  Copyright (C) 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--  University of Applied Sciences Rapperswil
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Command_Line;
with Ada.Exceptions;
with Ada.Text_IO;
with Ada.Strings.Fixed;

with ZKP.Elections;
with ZKP.Instructions;
with ZKP.OS;

procedure Vote
is
   use ZKP.Elections;
   use ZKP.Instructions;

   Output_Prefix : constant String := "obj/ballot-";

   E : constant Election_Type
     := Load_File (Filename => "data/public_key.json");
   Instr : constant Instruction_Type
     := Load_File (Filename => "data/voters.json");

   Current_Voter : Natural := 1;

   procedure Do_Vote
     (Voter_ID : String;
      Choice   : Natural;
      Cheat    : Boolean);
   --  Cast vote with given parameters and store to file.

   procedure Do_Vote
     (Voter_ID : String;
      Choice   : Natural;
      Cheat    : Boolean)
   is
      Nr_Str : constant String
        := Ada.Strings.Fixed.Trim (Source => Current_Voter'Img,
                                   Side   => Ada.Strings.Left);
      F_Name : constant String := Output_Prefix & Nr_Str & ".json";
   begin
      declare
         B : constant Ballot_Type
           := Cast_Vote (Election => E,
                         Voter_ID => Voter_ID,
                         Choice   => Choice,
                         Cheat    => Cheat);
      begin
         ZKP.OS.Write_File
           (Content  => To_JSON_String (Ballot => B),
            Filename => F_Name);
      end;

      Ada.Text_IO.Put_Line ("Stored vote to file " & F_Name);
      Current_Voter := Current_Voter + 1;
   end Do_Vote;
begin
   Iterate (Instruction => Instr,
            Process     => Do_Vote'Access);

   Ada.Command_Line.Set_Exit_Status (Code => Ada.Command_Line.Success);

exception
   when E : others =>
      Ada.Text_IO.Put_Line ("Processing error:");
      Ada.Text_IO.Put_Line (Ada.Exceptions.Exception_Information (X => E));
      Ada.Command_Line.Set_Exit_Status (Code => Ada.Command_Line.Failure);
end Vote;
