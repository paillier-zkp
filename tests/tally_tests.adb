--
--  Copyright (C) 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--  University of Applied Sciences Rapperswil
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Strings.Unbounded;

with ZKP.Elections.Mock;
with ZKP.Registry;
with ZKP.Tally;

package body Tally_Tests is

   use Ahven;
   use ZKP;
   use ZKP.Tally;

   function U (S : String) return Ada.Strings.Unbounded.Unbounded_String
               renames Ada.Strings.Unbounded.To_Unbounded_String;

   Ref_Election_File : constant String := "data/election_1536_1_2.json";
   --  Reference election file in JSON format.

   Ref_Registry_File : constant String := "data/registry.json";
   --  Reference shareholder registry file in JSON format.

   -------------------------------------------------------------------------

   procedure Add_Ballots
   is
      use ZKP.Elections;

      Ref_Tally1_Str : constant String := "bf949b";
      Ref_Tally2_Str : constant String := "11727c0";
      Ref_Tally3_Str : constant String := "6adbae";

      E  : constant Elections.Election_Type
        := Create
          (ID           => "Test_Election",
           Description  => "Unweighted test election",
           Candidates   => (0 => U ("C1"), 1 => U ("C2")),
           N            => "10F7",
           G            => "10F8",
           Exponent     => 6);
      R  : Registry.Shareholder_Registry_Type;
      B1 : constant Ballot_Type := Elections.Mock.Create_Ballot
        (Election_ID => Get_ID (Election => E),
         Voter_ID    => "V1",
         Vote        => "BF949B",
         Ak          => (0 => <>, 1 => <>),
         Ek          => (0 => <>, 1 => <>),
         Zk          => (0 => <>, 1 => <>));
      B2 : constant Ballot_Type := Elections.Mock.Create_Ballot
        (Election_ID => Get_ID (Election => E),
         Voter_ID    => "V2",
         Vote        => "2104D3",
         Ak          => (0 => <>, 1 => <>),
         Ek          => (0 => <>, 1 => <>),
         Zk          => (0 => <>, 1 => <>));
      B3 : constant Ballot_Type := Elections.Mock.Create_Ballot
        (Election_ID => Get_ID (Election => E),
         Voter_ID    => "V3",
         Vote        => "8ADF87",
         Ak          => (0 => <>, 1 => <>),
         Ek          => (0 => <>, 1 => <>),
         Zk          => (0 => <>, 1 => <>));
   begin

      --  Reference values generated using [1] with the following settings:
      --  p = 101, q = 43, message base = 64, random seed = 6,
      --  default choices for candidate selections; Ballots of voters V1-3
      --  used for tallying.
      --
      --  [1] - http://security.hsr.ch/msevote/paillier

      Registry.Add_Shareholder (Registry => R,
                                Name     => "V1",
                                Shares   => 1);
      Registry.Add_Shareholder (Registry => R,
                                Name     => "V2",
                                Shares   => 1);
      Registry.Add_Shareholder (Registry => R,
                                Name     => "V3",
                                Shares   => 1);

      declare
         T  : Tally_Type := Create_Tally (Election    => E,
                                          SH_Registry => R);
      begin
         Add_Ballot (Tally  => T,
                     Ballot => B1);
         Assert (Condition => Get_Value (Tally => T) = Ref_Tally1_Str,
                 Message   => "Tally value mismatch (1)");

         Add_Ballot (Tally  => T,
                     Ballot => B2);
         Assert (Condition => Get_Value (Tally => T) = Ref_Tally2_Str,
                 Message   => "Tally value mismatch (2)");

         Add_Ballot (Tally  => T,
                     Ballot => B3);
         Assert (Condition => Get_Value (Tally => T) = Ref_Tally3_Str,
                 Message   => "Tally value mismatch (3)");
      end;
   end Add_Ballots;

   -------------------------------------------------------------------------

   procedure Add_Ballots_Duplicate
   is
      use ZKP.Elections;

      E : constant Elections.Election_Type
        := Elections.Load_File (Filename => Ref_Election_File);
      R : constant Registry.Shareholder_Registry_Type
        := Registry.Load_File (Filename => Ref_Registry_File);
      B1 : constant Ballot_Type := Elections.Mock.Create_Ballot
        (Election_ID => Get_ID (Election => E),
         Voter_ID    => "Mister Tie",
         Vote        => "BF949B",
         Ak          => (0 => <>, 1 => <>),
         Ek          => (0 => <>, 1 => <>),
         Zk          => (0 => <>, 1 => <>));
      T : Tally_Type := Create_Tally (Election    => E,
                                      SH_Registry => R);
   begin
      Add_Ballot (Tally  => T,
                  Ballot => B1);
      Add_Ballot (Tally  => T,
                  Ballot => B1);
      Fail (Message => "Exception expected");

   exception
      when Duplicate_Voter => null;
   end Add_Ballots_Duplicate;

   -------------------------------------------------------------------------

   procedure Create_Tally
   is
      E : constant Elections.Election_Type
        := Elections.Load_File (Filename => Ref_Election_File);
      R : constant Registry.Shareholder_Registry_Type
        := Registry.Load_File (Filename => Ref_Registry_File);
      T : constant Tally_Type := Create_Tally (Election    => E,
                                               SH_Registry => R);
   begin
      Assert (Condition => Get_Election_ID (Tally => T)
              = Elections.Get_ID (Election => E),
              Message   => "Election ID mismatch");
      Assert (Condition => Get_Value (Tally => T) = "0",
              Message   => "Value mismatch");
   end Create_Tally;

   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "Tally related tests");
      T.Add_Test_Routine
        (Routine => Create_Tally'Access,
         Name    => "Create tally instance");
      T.Add_Test_Routine
        (Routine => Tally_To_JSON'Access,
         Name    => "Tally to JSON conversion");
      T.Add_Test_Routine
        (Routine => Add_Ballots'Access,
         Name    => "Tally ballots");
      T.Add_Test_Routine
        (Routine => Add_Ballots_Duplicate'Access,
         Name    => "Tally ballots (duplicate)");
   end Initialize;

   -------------------------------------------------------------------------

   procedure Tally_To_JSON
   is
      Ref_JSON_Str : constant String
        := "{""election_id"":""Election 1536-1-2"", ""ct"":""0""}";

      E : constant Elections.Election_Type
        := Elections.Load_File (Filename => Ref_Election_File);
      R : constant Registry.Shareholder_Registry_Type
        := Registry.Load_File (Filename => Ref_Registry_File);
      T : constant Tally_Type := Create_Tally (Election    => E,
                                               SH_Registry => R);
   begin
      Assert (Condition => To_JSON_String (Tally => T) = Ref_JSON_Str,
              Message   => "JSON string mismatch");
   end Tally_To_JSON;

end Tally_Tests;
