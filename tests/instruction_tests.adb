--
--  Copyright (C) 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--  University of Applied Sciences Rapperswil
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Strings.Unbounded;

with ZKP.Instructions;

package body Instruction_Tests is

   use Ada.Strings.Unbounded;
   use Ahven;
   use ZKP;
   use ZKP.Instructions;

   function U (S : String) return Ada.Strings.Unbounded.Unbounded_String
               renames Ada.Strings.Unbounded.To_Unbounded_String;

   Ref_Instructions_File : constant String := "data/instructions.json";
   --  Reference instructions file in JSON format.

   Ref_Election_ID       : constant String := "Election 1536-1-2";
   --  Reference election ID.

   subtype Ref_Voter_Range is Natural range 1 .. 3;

   Ref_Voter_IDs         : constant array (Ref_Voter_Range) of Unbounded_String
     := (U ("Mister Suite"),
         U ("Mister Tie"),
         U ("Miss Costume"));
   --  Reference voter ids.

   Ref_Votes             : constant array (Ref_Voter_Range) of Natural
     := (2, 1, 0);
   --  Reference votes.

   Ref_Cheat_Flags       : constant array (Ref_Voter_Range) of Boolean
     := (False, True, False);

   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "Voting instruction related tests");
      T.Add_Test_Routine
        (Routine => Load_Instruction'Access,
         Name    => "Load instruction from file");
      T.Add_Test_Routine
        (Routine => Iteration'Access,
         Name    => "Instruction iteration");
   end Initialize;

   -------------------------------------------------------------------------

   procedure Iteration
   is
      I   : constant Instruction_Type
        := Load_File (Filename => Ref_Instructions_File);
      Idx : Natural := Ref_Voter_Range'First;

      procedure Assert_Testdata
        (Voter_ID : String;
         Choice   : Natural;
         Cheat    : Boolean);
      --  Verify loaded data at current index.

      procedure Assert_Testdata
        (Voter_ID : String;
         Choice   : Natural;
         Cheat    : Boolean)
      is
      begin
         Assert (Condition => Voter_ID = To_String (Ref_Voter_IDs (Idx)),
                 Message   => "Expected voter id "
                 & To_String (Ref_Voter_IDs (Idx)));
         Assert (Condition => Choice = Ref_Votes (Idx),
                 Message   => "Expected vote" & Ref_Votes (Idx)'Img);
         Assert (Condition => Cheat = Ref_Cheat_Flags (Idx),
                 Message   => "Expected cheat flag "
                 & Ref_Cheat_Flags (Idx)'Img);

         Idx := Idx + 1;
      end Assert_Testdata;
   begin
      Iterate (Instruction => I,
               Process     => Assert_Testdata'Access);
      Assert (Condition => Idx = Natural (Ref_Voter_Range'Last) + 1,
              Message   => "Iterate process call mismatch");
   end Iteration;

   -------------------------------------------------------------------------

   procedure Load_Instruction
   is
      I : constant Instruction_Type
        := Load_File (Filename => Ref_Instructions_File);
   begin
      Assert (Condition => Get_Election_ID (Instruction => I)
              = Ref_Election_ID,
              Message   => "Election ID mismatch");
      Assert (Condition => Get_Vote_Count (Instruction => I)
              = Ref_Voter_Range'Last,
              Message   => "Vote count mismatch");
   end Load_Instruction;

end Instruction_Tests;
