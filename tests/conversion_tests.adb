--
--  Copyright (C) 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--  University of Applied Sciences Rapperswil
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Interfaces.C;

with GMP.Binding;

with ZKP.Utils;

package body Conversion_Tests is

   use Ahven;
   use ZKP.Utils;

   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "Conversion tests");
      T.Add_Test_Routine
        (Routine => To_Bignum_Conversion'Access,
         Name    => "Hex string to bignum");
   end Initialize;

   -------------------------------------------------------------------------

   procedure To_Bignum_Conversion
   is
      use type Interfaces.C.unsigned_long;

      B : GMP.Binding.Mpz_T;
   begin
      B := To_Bignum (Hex_Str => "378100cf");
      Assert (Condition => GMP.Binding.Mpz_Get_Ui (Op => B) = 931201231,
              Message   => "Invalid value");
      GMP.Binding.Mpz_Clear (Integer => B);

      begin
         B := To_Bignum (Hex_Str => "foobar");
         Fail (Message => "Exception expected");

      exception
         when Conversion_Error => null;
      end;
   end To_Bignum_Conversion;

end Conversion_Tests;
