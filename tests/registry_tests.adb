--
--  Copyright (C) 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--  University of Applied Sciences Rapperswil
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with ZKP.Registry;

package body Registry_Tests is

   use Ahven;
   use ZKP;
   use ZKP.Registry;

   -------------------------------------------------------------------------

   procedure Add_SH_To_Registry
   is
      SH_Name     : constant String       := "Mister Suit";
      Share_Count : constant Long_Integer := 152334987;
      R           : Shareholder_Registry_Type;
   begin
      Assert (Condition => Size (Registry => R) = 0,
              Message   => "Registry not empty");

      declare
         Count : Long_Integer;
         pragma Unreferenced (Count);
      begin
         Count := Get_Share_Count
           (Registry => R,
            Name     => SH_Name);

         Fail (Message => "Expected shareholder not present exception");

      exception
         when Shareholder_Not_Present => null;
      end;

      Add_Shareholder (Registry => R,
                       Name     => SH_Name,
                       Shares   => Share_Count);
      Assert (Condition => Size (Registry => R) = 1,
              Message   => "Registry still empty");
      Assert (Condition => Get_Share_Count
              (Registry => R,
               Name     => SH_Name) = Share_Count,
              Message   => "Share count mismatch");

      begin
         Add_Shareholder (Registry => R,
                          Name     => SH_Name,
                          Shares   => Share_Count);

         Fail (Message => "Expected shareholder present exception");

      exception
         when Shareholder_Present => null;
      end;
   end Add_SH_To_Registry;

   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "Shareholder registry tests");
      T.Add_Test_Routine
        (Routine => Add_SH_To_Registry'Access,
         Name    => "Add shareholder to registry");
      T.Add_Test_Routine
        (Routine => Load_From_File'Access,
         Name    => "Load registry data from file");
      T.Add_Test_Routine
        (Routine => Voter_Existence'Access,
         Name    => "Verify contains operation");
   end Initialize;

   -------------------------------------------------------------------------

   procedure Load_From_File
   is
      R : constant Shareholder_Registry_Type := Load_File
        (Filename => "data/registry.json");
   begin
      Assert (Condition => Size (Registry => R) = 2,
              Message   => "Registry size mismatch");
      Assert (Condition => Get_Share_Count
              (Registry => R,
               Name     => "Mister Suit") = 1868734,
              Message   => "Share count mismatch (Suit)");
      Assert (Condition => Get_Share_Count
              (Registry => R,
               Name     => "Mister Tie") = 234089,
              Message   => "Share count mismatch (Tie)");
   end Load_From_File;

   -------------------------------------------------------------------------

   procedure Voter_Existence
   is
      R : constant Shareholder_Registry_Type := Load_File
        (Filename => "data/registry.json");
   begin
      Assert (Condition => Contains (Registry => R,
                                     Name     => "Mister Suit"),
              Message   => "Does not contain 'Mister Suite'");

      Assert (Condition => not Contains (Registry => R,
                                     Name     => "nonexistent"),
              Message   => "Contains 'nonexistent'");
   end Voter_Existence;

end Registry_Tests;
