--
--  Copyright (C) 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--  University of Applied Sciences Rapperswil
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Interfaces.C;

with GMP.Binding;

with ZKP.Random;

package body Random_Tests is

   use Ahven;
   use GMP.Binding;
   use ZKP;

   package IC renames Interfaces.C;

   -------------------------------------------------------------------------

   procedure Get_Array_Of_Random_Coprimes
   is
      N : Mpz_T;
   begin
      Mpz_Init_Set_Ui (Rop => N,
                       Op  => 12312312);

      declare
         Coprimes : Mpz_T_Array := Random.Get_Coprimes
           (Number => N,
            Count  => 6);
      begin
         Assert (Condition => Coprimes'Length = 6,
                 Message   => "Invalid number of coprimes");
         Mpz_Clear (Integers => Coprimes);
      end;

      Mpz_Clear (Integer => N);
   end Get_Array_Of_Random_Coprimes;

   -------------------------------------------------------------------------

   procedure Get_Array_Of_Random_Numbers
   is
      Rs : Mpz_T_Array := Random.Get (Bit_Size => 128,
                                      Count    => 7);
   begin
      Assert (Condition => Rs'Length = 7,
              Message   => "Invalid array length");
      Mpz_Clear (Integers => Rs);
   end Get_Array_Of_Random_Numbers;

   -------------------------------------------------------------------------

   procedure Get_Random_Coprimes
   is
      use type IC.size_t;
      use type IC.int;
      use type IC.unsigned_long;

      Count     : constant := 1024;
      N, R, Gcd : Mpz_T;
   begin
      Mpz_Init (Integer => Gcd);
      Mpz_Init_Set_Ui (Rop => N,
                       Op  => 3429496729);

      for I in 1 .. Count loop
         R := Random.Get_Coprime (Number => N);
         Mpz_Gcd (Rop => Gcd,
                  Op1 => N,
                  Op2 => R);
         Assert (Condition => Mpz_Get_Ui (Op => Gcd) = 1,
                 Message   => "Number not coprime");
         Assert (Condition => Mpz_Cmp (Op1 => N, Op2 => R) >= 0,
                 Message   => "R > N");
         Mpz_Clear (Integer => R);
      end loop;

      Mpz_Clear (Integer => N);
      Mpz_Clear (Integer => Gcd);
   end Get_Random_Coprimes;

   -------------------------------------------------------------------------

   procedure Get_Random_Numbers
   is
      use type IC.size_t;
      use type IC.int;
      use type IC.unsigned_long;

      Count      : constant := 256;
      Previous_R : Mpz_T_Array (1 .. Count);
      R          : Mpz_T;

      function Is_Unique
        (Current : Mpz_T;
         Index   : Positive)
         return Boolean;
      --  Return True if the current number is not the same as a previous
      --  number.

      function Is_Unique
        (Current : Mpz_T;
         Index   : Positive)
         return Boolean
      is
      begin
         for P in Integer range 1 .. Index - 1 loop
            if Mpz_Cmp (Op1 => Previous_R (P),
                        Op2 => Current) = 0
            then
               return False;
            end if;
         end loop;

         return True;
      end Is_Unique;
   begin
      for I in 1 .. Count loop
         R := Random.Get (Bit_Size => 512);
         Assert (Condition => Is_Unique
                 (Current => R, Index => I),
                 Message   => "Number not random");

         Previous_R (I) := R;
      end loop;

      Mpz_Clear (Integers => Previous_R);
   end Get_Random_Numbers;

   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "Random number generator tests");
      T.Add_Test_Routine
        (Routine => Get_Random_Numbers'Access,
         Name    => "Get random numbers");
      T.Add_Test_Routine
        (Routine => Get_Array_Of_Random_Numbers'Access,
         Name    => "Get array of random numbers");
      T.Add_Test_Routine
        (Routine => Get_Random_Coprimes'Access,
         Name    => "Get random coprimes");
      T.Add_Test_Routine
        (Routine => Get_Array_Of_Random_Coprimes'Access,
         Name    => "Get array of random coprimes");
   end Initialize;

end Random_Tests;
