--
--  Copyright (C) 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--  University of Applied Sciences Rapperswil
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Interfaces.C;

with GMP.Binding;

with ZKP.Paillier;
with ZKP.Utils;

package body Paillier_Tests is

   use Ahven;
   use GMP.Binding;
   use ZKP;
   use ZKP.Paillier;

   package IC renames Interfaces.C;

   Empty_Mpz_Array : constant Mpz_T_Array (0 .. 4) := (others => <>);

   --  Reference challenge and response ek values calculated using the online
   --  JavaScript E-Voting Application developed at the University of Applied
   --  Science in Rapperswil [1].
   --
   --  [1] - http://security.hsr.ch/msevote/js_evote.html

   Ch_Str : constant String := "67b555d1300e2c7de8382a8fecc82e496be4101a294"
     & "79544d89c3dc3457b25f7698448600009a478b18531740609a285d57195a62b3538"
     & "019a43825f3d84521674e775150d10b31950de1a4a483649d0b45d3c9f9c894a205"
     & "3be4a4712a07c09";
   E0_Str : constant String := "6fa395094192767bc08ae7be29f46fcd8d24c37b32b"
     & "3eb715dd0216311ea4ca21e5c17372017453b5d4bbdb989aae28eacb314d289d917"
     & "d7bae7d2477cbdebb92c87df0e5ad56d07bf557deb84647091ebad1bd5a1b2049be"
     & "2e68a6d39279bce";
   E1_Str : constant String := "6572bc81c080b0c2acc6535c0419548b83bb4c58b94"
     & "14919a32d369c8202217acde466c2458c4115b18ec8dda71a5472d9c4cce5a4ce1e"
     & "40c706c5e10a32580c643f42df199df81d8edce0f29eaa36c9b231e12380ee681e0"
     & "8665c42b35d9da7";
   E2_Str : constant String := "129f04462dfb053f7ae6ef75beba69f05b0400463d5"
     & "260b9d79ee5c3b18eb7da7d43ca669a661e27a2aaaadcd5446b844ef9b3edfc8e01"
     & "e91854ea36b6940e50e4205327989d4df402abbb6c2527a275167e3fa679e8dd666"
     & "8716397261b4294";

   -------------------------------------------------------------------------

   procedure Calculate_256_Bit_Challenge
   is
      Ref_Challenge : constant String
        := "3b5dc36665153ba8d764ad20aa2a8654c1401048e8ce9eab42eed61fad2b7387";
      --  Reference SHA256 message digest computed using [1] with input:
      --     "My_Election1Hans Muster49631d4b7b5a5316457bacc3"
      --  and clearing the leading bit of the resulting hash.
      --
      --  [1] - http://www.xorbin.com/tools/sha256-hash-calculator

      C  : Mpz_T;
      Ak : Ak_Array (0 .. 3);
   begin
      Mpz_Init_Set_Ui (Rop => C,
                       Op  => 1231232331);
      Mpz_Init_Set_Ui (Rop => Ak (0),
                       Op  => 123);
      Mpz_Init_Set_Ui (Rop => Ak (1),
                       Op  => 23123);
      Mpz_Init_Set_Ui (Rop => Ak (2),
                       Op  => 91223);
      Mpz_Init_Set_Ui (Rop => Ak (3),
                       Op  => 765123);

      declare
         use type IC.size_t;
         use type IC.int;

         Challenge : Mpz_T := Compute_Challenge
           (Election_Id => "My_Election1",
            Voter_Id    => "Hans Muster",
            C           => C,
            Commitment  => Ak,
            Size        => 256);
         Ch_Str    : IC.char_array (1 .. 65);
      begin
         Mpz_Get_Str (Str  => Ch_Str,
                      Base => 16,
                      Op   => Challenge);

         Assert (Condition => Mpz_Sizeinbase
                 (Op   => Challenge,
                  Base => 2) < 256,
                 Message   => "Challenge length mismatch");
         Assert (Condition => IC.To_Ada (Ch_Str) = Ref_Challenge,
                 Message   => "Challenge incorrect");

         Mpz_Clear (Integer => Challenge);
      end;

      Mpz_Clear (Integers => Ak);
      Mpz_Clear (Integer => C);
   end Calculate_256_Bit_Challenge;

   -------------------------------------------------------------------------

   procedure Calculate_768_Bit_Challenge
   is
      Ref_Challenge : constant String := "3b5dc36665153ba8d764ad20aa2a8654c140"
        & "1048e8ce9eab42eed61fad2b738743c69d8c60606caaa7f15f0b76c33d5c5e34bfb"
        & "f106e002977efe083d8e622bc5d2f3e3dfeef3f88e11201ea3d6a83814ce87a9b99"
        & "981e1d72273298eb59c1fa";

      C  : Mpz_T;
      Ak : Ak_Array (0 .. 3);
   begin
      Mpz_Init_Set_Ui (Rop => C,
                       Op  => 1231232331);
      Mpz_Init_Set_Ui (Rop => Ak (0),
                       Op  => 123);
      Mpz_Init_Set_Ui (Rop => Ak (1),
                       Op  => 23123);
      Mpz_Init_Set_Ui (Rop => Ak (2),
                       Op  => 91223);
      Mpz_Init_Set_Ui (Rop => Ak (3),
                       Op  => 765123);

      declare
         use type IC.size_t;

         Challenge : Mpz_T := Compute_Challenge
           (Election_Id => "My_Election1",
            Voter_Id    => "Hans Muster",
            C           => C,
            Commitment  => Ak,
            Size        => 768);
         Ch_Str    : IC.char_array (1 .. 193);
      begin
         Mpz_Get_Str (Str  => Ch_Str,
                      Base => 16,
                      Op   => Challenge);

         Assert (Condition => Mpz_Sizeinbase
                 (Op   => Challenge,
                  Base => 2) < 768,
                 Message   => "Challenge length mismatch");
         Assert (Condition => IC.To_Ada (Ch_Str) = Ref_Challenge,
                 Message   => "Challenge incorrect");

         Mpz_Clear (Integer => Challenge);
      end;

      Mpz_Clear (Integers => Ak);
      Mpz_Clear (Integer => C);
   end Calculate_768_Bit_Challenge;

   -------------------------------------------------------------------------

   procedure Calculate_Ballots_Uk
   is
      Ex, N, N2, C, G : Mpz_T;
   begin
      Mpz_Init_Set_Ui (Rop => N,
                       Op  => 33);
      Mpz_Init_Set_Ui (Rop => Ex,
                       Op  => 4);
      declare
         Mk : Mk_Array := Create_Messages_Mk
           (N          => N,
            Base       => Ex,
            Candidates => 4);
      begin
         Mpz_Init_Set_Ui (Rop => N2,
                          Op  => 1089);
         Mpz_Init_Set_Ui (Rop => C,
                          Op  => 905);
         Mpz_Init_Set_Ui (Rop => G,
                          Op  => 166);

         declare
            use type IC.unsigned_long;

            Uk : Uk_Array := Compute_Uk
              (Mk => Mk,
               G  => G,
               N2 => N2,
               C  => C);
         begin
            Assert (Condition => Mpz_Get_Ui (Op => Uk (0)) = 905,
                    Message   => "u0 incorrect:" & Mpz_Get_Ui
                      (Op => Uk (0))'Img);
            Assert (Condition => Mpz_Get_Ui (Op => Uk (1)) = 773,
                    Message   => "u1 incorrect:" & Mpz_Get_Ui
                      (Op => Uk (1))'Img);
            Assert (Condition => Mpz_Get_Ui (Op => Uk (2)) = 377,
                    Message   => "u2 incorrect:" & Mpz_Get_Ui
                      (Op => Uk (2))'Img);
            Assert (Condition => Mpz_Get_Ui (Op => Uk (3)) = 971,
                    Message   => "u3 incorrect:" & Mpz_Get_Ui
                      (Op => Uk (3))'Img);

            Mpz_Clear (Integers => Uk);
         end;

         Mpz_Clear (Integers => Mk);
      end;

      Mpz_Set_Ui (Rop => N,
                  Op  => 14111);
      Mpz_Set_Ui (Rop => Ex,
                  Op  => 64);
      declare
         Mk : Mk_Array := Create_Messages_Mk
           (N          => N,
            Base       => Ex,
            Candidates => 4);
      begin
         Mpz_Set_Ui (Rop => N2,
                     Op  => 199120321);
         Mpz_Set_Ui (Rop => C,
                     Op  => 125090643);
         Mpz_Set_Ui (Rop => G,
                     Op  => 45199549);

         declare
            use type IC.unsigned_long;

            Uk : Uk_Array := Compute_Uk
              (Mk => Mk,
               G  => G,
               N2 => N2,
               C  => C);
         begin
            Assert (Condition => Mpz_Get_Ui (Op => Uk (0)) = 125090643,
                    Message   => "u0 incorrect:" & Mpz_Get_Ui
                      (Op => Uk (0))'Img);
            Assert (Condition => Mpz_Get_Ui (Op => Uk (1)) = 103452359,
                    Message   => "u1 incorrect:" & Mpz_Get_Ui
                      (Op => Uk (1))'Img);
            Assert (Condition => Mpz_Get_Ui (Op => Uk (2)) = 85586479,
                    Message   => "u2 incorrect:" & Mpz_Get_Ui
                      (Op => Uk (2))'Img);
            Assert (Condition => Mpz_Get_Ui (Op => Uk (3)) = 127981871,
                    Message   => "u3 incorrect:" & Mpz_Get_Ui
                      (Op => Uk (3))'Img);

            Mpz_Clear (Integers => Uk);
         end;

         Mpz_Clear (Integers => Mk);
      end;

      Mpz_Clear (Integer => Ex);
      Mpz_Clear (Integer => N);
      Mpz_Clear (Integer => N2);
      Mpz_Clear (Integer => C);
      Mpz_Clear (Integer => G);
   end Calculate_Ballots_Uk;

   -------------------------------------------------------------------------

   procedure Calculate_Ballots_Uk_No_Inverse
   is
      Ex, N, N2, C, G : Mpz_T;
      Mk              : Mk_Array (0 .. 3);
   begin
      Mpz_Init_Set_Ui (Rop => N,
                       Op  => 10);
      Mpz_Init_Set_Ui (Rop => Ex,
                       Op  => 4);
      Mk := Create_Messages_Mk
        (N          => N,
         Base       => Ex,
         Candidates => 4);

      Mpz_Init_Set_Ui (Rop => N2,
                       Op  => 1088);
      Mpz_Init_Set_Ui (Rop => C,
                       Op  => 905);
      Mpz_Init_Set_Ui (Rop => G,
                       Op  => 166);

      begin
         declare
            use type IC.unsigned_long;

            Uk : constant Uk_Array := Compute_Uk
              (Mk => Mk,
               G  => G,
               N2 => N2,
               C  => C);
            pragma Unreferenced (Uk);
         begin
            Fail (Message => "Exception expected");
         end;

      exception
         when Calculation_Error => null;
      end;

      Mpz_Clear (Integers => Mk);
      Mpz_Clear (Integer => Ex);
      Mpz_Clear (Integer => N);
      Mpz_Clear (Integer => N2);
      Mpz_Clear (Integer => C);
      Mpz_Clear (Integer => G);
   end Calculate_Ballots_Uk_No_Inverse;

   -------------------------------------------------------------------------

   procedure Calculate_Commitment_Ak
   is
      Ex, N, N2, C, G : Mpz_T;
      Ek, Zk          : Mpz_T_Array (0 .. 3);
      Mk              : Mk_Array (Ek'Range);
   begin

      --  Reference values generated using [1] with the following settings:
      --  p = 131, q = 107, message base = 4, random seed = 6,
      --  default choices for candidate selections; V2 used as voter.
      --
      --  [1] - http://security.hsr.ch/msevote/paillier

      Mpz_Init_Set_Ui (Rop => N,
                       Op  => 14017);
      Mpz_Init_Set_Ui (Rop => N2,
                       Op  => 196476289);
      Mpz_Init_Set_Ui (Rop => C,
                       Op  => 73815210);
      Mpz_Init_Set_Ui (Rop => G,
                       Op  => 191367792);

      Mpz_Init_Set_Ui (Rop => Ex,
                       Op  => 4);
      Mk := Create_Messages_Mk
        (N          => N,
         Base       => Ex,
         Candidates => 4);

      Mpz_Init_Set_Ui (Rop => Ek (0),
                       Op  => 40);
      Mpz_Init_Set_Ui (Rop => Ek (1),
                       Op  => 57);
      Mpz_Init_Set_Ui (Rop => Ek (2),
                       Op  => 33);
      Mpz_Init_Set_Ui (Rop => Ek (3),
                       Op  => 17);

      Mpz_Init_Set_Ui (Rop => Zk (0),
                       Op  => 3263);
      Mpz_Init_Set_Ui (Rop => Zk (1),
                       Op  => 4879);
      Mpz_Init_Set_Ui (Rop => Zk (2),
                       Op  => 11913);
      Mpz_Init_Set_Ui (Rop => Zk (3),
                       Op  => 3299);

      declare
         use type IC.unsigned_long;

         Uk : Uk_Array := Compute_Uk
           (Mk => Mk,
            G  => G,
            N2 => N2,
            C  => C);
         Ak : Ak_Array := Compute_Commitment_Ak
           (Choice => 2,
            Ek     => Ek,
            Zk     => Zk,
            Uk     => Uk,
            N      => N,
            N2     => N2);
      begin
         Assert (Condition => Mpz_Get_Ui (Op => Ak (0)) = 7977577,
                 Message   => "a0 incorrect:" & Mpz_Get_Ui
                   (Op => Ak (0))'Img);
         Assert (Condition => Mpz_Get_Ui (Op => Ak (1)) = 194359099,
                 Message   => "a1 incorrect:" & Mpz_Get_Ui
                   (Op => Ak (1))'Img);
         Assert (Condition => Mpz_Get_Ui (Op => Ak (2)) = 57715104,
                 Message   => "a2 incorrect:" & Mpz_Get_Ui
                   (Op => Ak (2))'Img);
         Assert (Condition => Mpz_Get_Ui (Op => Ak (3)) = 101861230,
                 Message   => "a3 incorrect:" & Mpz_Get_Ui
                   (Op => Ak (3))'Img);

         Mpz_Clear (Integers => Uk);
         Mpz_Clear (Integers => Ak);
      end;

      Mpz_Clear (Integers => Ek);
      Mpz_Clear (Integers => Zk);
      Mpz_Clear (Integers => Mk);

      Mpz_Clear (Integer => Ex);
      Mpz_Clear (Integer => N);
      Mpz_Clear (Integer => N2);
      Mpz_Clear (Integer => C);
      Mpz_Clear (Integer => G);
   end Calculate_Commitment_Ak;

   -------------------------------------------------------------------------

   procedure Calculate_Commitment_Ak_Invalid_Choice
   is
      Dummy : Mpz_T;
   begin
      Mpz_Init (Integer => Dummy);

      begin
         declare
            Ak : Ak_Array := Compute_Commitment_Ak
              (Choice => 5,
               Ek     => Empty_Mpz_Array,
               Zk     => Empty_Mpz_Array,
               Uk     => Uk_Array (Empty_Mpz_Array),
               N      => Dummy,
               N2     => Dummy);
            pragma Unreferenced (Ak);
         begin
            Fail (Message => "Exception expected");
         end;

      exception
         when Calculation_Error => null;
      end;

      Mpz_Clear (Integer => Dummy);
   end Calculate_Commitment_Ak_Invalid_Choice;

   -------------------------------------------------------------------------

   procedure Calculate_Commitment_Ak_No_Inverse
   is
      N, N2  : Mpz_T;
      Ek, Zk : Mpz_T_Array (0 .. 3);
      Uk     : Uk_Array (0 .. 3);
   begin
      Mpz_Init_Set_Ui (Rop => N,
                       Op  => 32);
      Mpz_Init_Set_Ui (Rop => N2,
                       Op  => 1024);

      Mpz_Init_Set_Ui (Rop => Ek (0),
                       Op  => 124);
      Mpz_Init_Set_Ui (Rop => Zk (0),
                       Op  => 12);
      Mpz_Init_Set_Ui (Rop => Uk (0),
                       Op  => 3262);

      declare
         Ak : Ak_Array (0 .. 3);
         pragma Unreferenced (Ak);
      begin
         Ak := Compute_Commitment_Ak
           (Choice => 3,
            Ek     => Ek,
            Zk     => Zk,
            Uk     => Uk,
            N      => N,
            N2     => N2);
         Fail (Message => "Exception expected");

      exception
         when Calculation_Error => null;
      end;

      Mpz_Clear (Integer => Ek (0));
      Mpz_Clear (Integer => Zk (0));
      Mpz_Clear (Integer => Uk (0));
      Mpz_Clear (Integer => N);
      Mpz_Clear (Integer => N2);
   end Calculate_Commitment_Ak_No_Inverse;

   -------------------------------------------------------------------------

   procedure Calculate_Response_Ek
   is
      use type IC.int;

      Ch, Ei : Mpz_T;
      E      : Mpz_T_Array (0 .. 2);
      Ek     : Ek_Array (0 .. 2);
   begin
      Mpz_Init (Integer => E (0));
      E (1) := Utils.To_Bignum (Hex_Str => E1_Str);
      E (2) := Utils.To_Bignum (Hex_Str => E2_Str);
      Ei    := Utils.To_Bignum (Hex_Str => E0_Str);

      Ch := Utils.To_Bignum (Hex_Str => Ch_Str);

      Ek := Compute_Response_Ek (Choice    => 0,
                                 Challenge => Ch,
                                 Size      => 767,
                                 Random_E  => E);

      Assert (Condition => Mpz_Cmp
              (Op1 => Ek (0),
               Op2 => Ei) = 0,
              Message   => "e0 incorrect");
      Assert (Condition => Mpz_Cmp
              (Op1 => Ek (1),
               Op2 => E (1)) = 0,
              Message   => "e1 incorrect");
      Assert (Condition => Mpz_Cmp
              (Op1 => Ek (2),
               Op2 => E (2)) = 0,
              Message   => "e2 incorrect");

      Mpz_Clear (Integer => Ch);
      Mpz_Clear (Integer => Ei);
      Mpz_Clear (Integers => E);
      Mpz_Clear (Integers => Ek);
   end Calculate_Response_Ek;

   -------------------------------------------------------------------------

   procedure Calculate_Response_Ek_Invalid_Choice
   is
      Dummy : Mpz_T;
   begin
      Mpz_Init (Integer => Dummy);

      begin
         declare
            Ek : Ek_Array := Compute_Response_Ek
              (Choice    => 5,
               Challenge => Dummy,
               Size      => 258,
               Random_E  => Empty_Mpz_Array);
            pragma Unreferenced (Ek);
         begin
            Fail (Message => "Exception expected");
         end;

      exception
         when Calculation_Error => null;
      end;

      Mpz_Clear (Integer => Dummy);
   end Calculate_Response_Ek_Invalid_Choice;

   -------------------------------------------------------------------------

   procedure Calculate_Response_Zk
   is
      use type IC.unsigned_long;

      R, N : Mpz_T;
      Z    : Mpz_T_Array (0 .. 3);
      Ek   : Ek_Array (0 .. 3);
      Zk   : Zk_Array (0 .. 3);
   begin

      --  Reference values generated using [1] with the following settings:
      --  p = 139, q = 137, message base = 4, random seed = 6,
      --  default choices for candidate selections; V2 used as voter.
      --
      --  [1] - http://security.hsr.ch/msevote/paillier

      Mpz_Init_Set_Ui (Rop => R,
                       Op  => 18437);
      Mpz_Init_Set_Ui (Rop => N,
                       Op  => 19043);

      Mpz_Init_Set_Ui (Rop => Ek (0),
                       Op  => 83);
      Mpz_Init_Set_Ui (Rop => Ek (1),
                       Op  => 125);
      Mpz_Init_Set_Ui (Rop => Ek (2),
                       Op  => 125);
      Mpz_Init_Set_Ui (Rop => Ek (3),
                       Op  => 63);

      Mpz_Init_Set_Ui (Rop => Z (0),
                       Op  => 10886);
      Mpz_Init_Set_Ui (Rop => Z (1),
                       Op  => 3255);
      Mpz_Init_Set_Ui (Rop => Z (2),
                       Op  => 4867);
      Mpz_Init_Set_Ui (Rop => Z (3),
                       Op  => 3291);

      Zk := Compute_Response_Zk
        (Choice   => 2,
         N        => N,
         R        => R,
         Random_Z => Z,
         Ek       => Ek);

      Assert (Condition => Mpz_Get_Ui (Op => Zk (0)) = 10886,
              Message   => "z0 incorrect");
      Assert (Condition => Mpz_Get_Ui (Op => Zk (1)) = 3255,
              Message   => "z1 incorrect");
      Assert (Condition => Mpz_Get_Ui (Op => Zk (2)) = 6347,
              Message   => "z2 incorrect");
      Assert (Condition => Mpz_Get_Ui (Op => Zk (3)) = 3291,
              Message   => "z3 incorrect");

      Mpz_Clear (Integer => R);
      Mpz_Clear (Integer => N);
      Mpz_Clear (Integers => Ek);
      Mpz_Clear (Integers => Z);
      Mpz_Clear (Integers => Zk);
   end Calculate_Response_Zk;

   -------------------------------------------------------------------------

   procedure Calculate_Response_Zk_Invalid_Choice
   is
      Dummy : Mpz_T;
   begin
      Mpz_Init (Integer => Dummy);

      begin
         declare
            Zk : Zk_Array := Compute_Response_Zk
              (Choice   => 5,
               N        => Dummy,
               R        => Dummy,
               Random_Z => Empty_Mpz_Array,
               Ek       => Ek_Array (Empty_Mpz_Array));
            pragma Unreferenced (Zk);
         begin
            Fail (Message => "Exception expected");
         end;

      exception
         when Calculation_Error => null;
      end;

      Mpz_Clear (Integer => Dummy);
   end Calculate_Response_Zk_Invalid_Choice;

   -------------------------------------------------------------------------

   procedure Create_Messages_Mk
   is
      use type IC.int;

      N, Base, Ref : Mpz_T;
      Candidates   : constant := 4;
   begin
      Mpz_Init_Set_Ui (Rop => N,
                       Op  => 33);
      Mpz_Init (Integer => Ref);
      Mpz_Init_Set_Ui (Rop => Base,
                       Op  => 4);

      declare
         use type IC.unsigned_long;

         Messages : Mk_Array := Create_Messages_Mk
           (N          => N,
            Base       => Base,
            Candidates => Candidates);
      begin
         Assert (Condition => Mpz_Get_Ui (Op => Messages (0)) = 0,
                 Message   => "m0 incorrect");
         for C in Positive range 1 .. Candidates - 1 loop
            Mpz_Pow_Ui (Rop  => Ref,
                        Base => Base,
                        Exp  => IC.unsigned_long (C - 1));
            Assert (Condition => Mpz_Cmp
                    (Op1 => Messages (C),
                     Op2 => Ref) = 0,
                    Message   => "Message" & C'Img & " incorrect:"
                    & Mpz_Get_Ui (Op => Messages (C))'Img & " /"
                    & Mpz_Get_Ui (Op => Ref)'Img);
         end loop;

         Mpz_Clear (Integers => Messages);
         Mpz_Clear (Integer => N);
         Mpz_Clear (Integer => Base);
         Mpz_Clear (Integer => Ref);
      end;
   end Create_Messages_Mk;

   -------------------------------------------------------------------------

   procedure Encrypt_Message
   is
      use type IC.unsigned_long;

      N_Ui : constant := 209;
      G_Ui : constant := 25244;
      M_Ui : constant := 6;
      R_Ui : constant := 56;
      C_Ui : constant := 37049;

      C, M, R, G, N, N2 : Mpz_T;
   begin
      Mpz_Init_Set_Ui (Rop => M,
                       Op  => M_Ui);
      Mpz_Init_Set_Ui (Rop => R,
                       Op  => R_Ui);
      Mpz_Init_Set_Ui (Rop => G,
                       Op  => G_Ui);
      Mpz_Init_Set_Ui (Rop => N,
                       Op  => N_Ui);

      Mpz_Init (Integer => N2);
      Mpz_Mul (Rop => N2,
               Op1 => N,
               Op2 => N);

      C := Encrypt
        (M  => M,
         R  => R,
         G  => G,
         N  => N,
         N2 => N2);

      Assert (Condition => C_Ui = Mpz_Get_Ui (Op => C),
              Message   => "Result incorrect");

      Mpz_Clear (Integer => M);
      Mpz_Clear (Integer => R);
      Mpz_Clear (Integer => G);
      Mpz_Clear (Integer => N);
      Mpz_Clear (Integer => N2);
      Mpz_Clear (Integer => C);
   end Encrypt_Message;

   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "Tests for Paillier package");
      T.Add_Test_Routine
        (Routine => Encrypt_Message'Access,
         Name    => "Encrypt message");
      T.Add_Test_Routine
        (Routine => Create_Messages_Mk'Access,
         Name    => "Create voting messages mk");
      T.Add_Test_Routine
        (Routine => Calculate_Ballots_Uk'Access,
         Name    => "Compute ballots uk");
      T.Add_Test_Routine
        (Routine => Calculate_Ballots_Uk_No_Inverse'Access,
         Name    => "Compute uk - no inverse element for g");
      T.Add_Test_Routine
        (Routine => Calculate_Commitment_Ak'Access,
         Name    => "Compute commitment ak");
      T.Add_Test_Routine
        (Routine => Calculate_Commitment_Ak_Invalid_Choice'Access,
         Name    => "Compute ak - invalid choice");
      T.Add_Test_Routine
        (Routine => Calculate_Commitment_Ak_No_Inverse'Access,
         Name    => "Compute ak - no inverse for uk^n in n^2");
      T.Add_Test_Routine
        (Routine => Calculate_256_Bit_Challenge'Access,
         Name    => "Compute 256 bit challenge");
      T.Add_Test_Routine
        (Routine => Calculate_768_Bit_Challenge'Access,
         Name    => "Compute 768 bit challenge");
      T.Add_Test_Routine
        (Routine => Calculate_Response_Ek'Access,
         Name    => "Compute response ek");
      T.Add_Test_Routine
        (Routine => Calculate_Response_Ek_Invalid_Choice'Access,
         Name    => "Compute ek - invalid choice");
      T.Add_Test_Routine
        (Routine => Calculate_Response_Zk'Access,
         Name    => "Compute response zk");
      T.Add_Test_Routine
        (Routine => Calculate_Response_Zk_Invalid_Choice'Access,
         Name    => "Compute zk - invalid choice");
      T.Add_Test_Routine
        (Routine => Verify_Challenge_Equals_Eksum'Access,
         Name    => "Verify response ek");
      T.Add_Test_Routine
        (Routine => Verify_Nth_Power_Valid'Access,
         Name    => "Verify n-th powers (valid)");
      T.Add_Test_Routine
        (Routine => Verify_Nth_Power_Cheat'Access,
         Name    => "Verify n-th powers (cheat)");
   end Initialize;

   -------------------------------------------------------------------------

   procedure Verify_Challenge_Equals_Eksum
   is
      Ch : Mpz_T;
      Ek : Ek_Array (0 .. 2);
   begin
      Ch     := Utils.To_Bignum (Hex_Str => Ch_Str);
      Ek (0) := Utils.To_Bignum (Hex_Str => E0_Str);
      Ek (1) := Utils.To_Bignum (Hex_Str => E1_Str);
      Ek (2) := Utils.To_Bignum (Hex_Str => E2_Str);

      Assert (Condition => Verify_Response_Ek
              (Challenge => Ch,
               Ek        => Ek),
              Message   => "Verification failed");

      Mpz_Clear (Integer => Ek (2));
      Ek (2) := Utils.To_Bignum (Hex_Str => "ababcdab");

      Assert (Condition => not Verify_Response_Ek
              (Challenge => Ch,
               Ek        => Ek),
              Message   => "False expected");

      Mpz_Clear (Integers => Ek);
      Mpz_Clear (Integer => Ch);
   end Verify_Challenge_Equals_Eksum;

   -------------------------------------------------------------------------

   procedure Verify_Nth_Power_Cheat
   is
      N      : Mpz_T;
      Uk     : Uk_Array (0 .. 3);
      Ak     : Ak_Array (0 .. 3);
      Ek     : Ek_Array (0 .. 3);
      Zk     : Zk_Array (0 .. 3);
      Result : Boolean := True;
      Index  : Integer;
   begin

      --  Reference values generated using [1] with the following settings:
      --  p = 139, q = 137, message base = 4, random seed = 6,
      --  default choices for candidate selections; V4 used as cheating voter
      --  (voting twice for candidate 3).
      --
      --  [1] - http://security.hsr.ch/msevote/paillier

      Mpz_Init_Set_Ui (Rop => N,
                       Op  => 19043);

      Mpz_Init_Set_Ui (Rop => Uk (0),
                       Op  => 56032396);
      Mpz_Init_Set_Ui (Rop => Uk (1),
                       Op  => 272802549);
      Mpz_Init_Set_Ui (Rop => Uk (2),
                       Op  => 279624198);
      Mpz_Init_Set_Ui (Rop => Uk (3),
                       Op  => 148627952);

      Mpz_Init_Set_Ui (Rop => Ak (0),
                       Op  => 351351);
      Mpz_Init_Set_Ui (Rop => Ak (1),
                       Op  => 297026730);
      Mpz_Init_Set_Ui (Rop => Ak (2),
                       Op  => 302177826);
      Mpz_Init_Set_Ui (Rop => Ak (3),
                       Op  => 38900736);

      Mpz_Init_Set_Ui (Rop => Ek (0),
                       Op  => 35);
      Mpz_Init_Set_Ui (Rop => Ek (1),
                       Op  => 103);
      Mpz_Init_Set_Ui (Rop => Ek (2),
                       Op  => 104);
      Mpz_Init_Set_Ui (Rop => Ek (3),
                       Op  => 88);

      Mpz_Init_Set_Ui (Rop => Zk (0),
                       Op  => 3466);
      Mpz_Init_Set_Ui (Rop => Zk (1),
                       Op  => 3862);
      Mpz_Init_Set_Ui (Rop => Zk (2),
                       Op  => 11146);
      Mpz_Init_Set_Ui (Rop => Zk (3),
                       Op  => 6297);

      Verify_Nth_Powers
        (Success  => Result,
         Err_Idx  => Index,
         Uk       => Uk,
         Ak       => Ak,
         Ek       => Ek,
         Zk       => Zk,
         N        => N);
      Assert (Condition => not Result,
              Message   => "Verification passed");
      Assert (Condition => Index = 3,
              Message   => "Index mismatch" & Index'Img);

      Mpz_Clear (Integer => N);
      Mpz_Clear (Integers => Uk);
      Mpz_Clear (Integers => Ak);
      Mpz_Clear (Integers => Ek);
      Mpz_Clear (Integers => Zk);
   end Verify_Nth_Power_Cheat;

   -------------------------------------------------------------------------

   procedure Verify_Nth_Power_Valid
   is
      N      : Mpz_T;
      Uk     : Uk_Array (0 .. 3);
      Ak     : Ak_Array (0 .. 3);
      Ek     : Ek_Array (0 .. 3);
      Zk     : Zk_Array (0 .. 3);
      Result : Boolean := False;
      Index  : Integer;
   begin

      --  Reference values generated using [1] with the following settings:
      --  p = 139, q = 137, message base = 4, random seed = 6,
      --  default choices for candidate selections; V1 used as voter.
      --
      --  [1] - http://security.hsr.ch/msevote/paillier

      Mpz_Init_Set_Ui (Rop => N,
                       Op  => 19043);

      Mpz_Init_Set_Ui (Rop => Uk (0),
                       Op  => 260974581);
      Mpz_Init_Set_Ui (Rop => Uk (1),
                       Op  => 275428336);
      Mpz_Init_Set_Ui (Rop => Uk (2),
                       Op  => 99858453);
      Mpz_Init_Set_Ui (Rop => Uk (3),
                       Op  => 247338080);

      Mpz_Init_Set_Ui (Rop => Ak (0),
                       Op  => 12135788);
      Mpz_Init_Set_Ui (Rop => Ak (1),
                       Op  => 353728867);
      Mpz_Init_Set_Ui (Rop => Ak (2),
                       Op  => 12619316);
      Mpz_Init_Set_Ui (Rop => Ak (3),
                       Op  => 49259333);

      Mpz_Init_Set_Ui (Rop => Ek (0),
                       Op  => 96);
      Mpz_Init_Set_Ui (Rop => Ek (1),
                       Op  => 30);
      Mpz_Init_Set_Ui (Rop => Ek (2),
                       Op  => 113);
      Mpz_Init_Set_Ui (Rop => Ek (3),
                       Op  => 6);

      Mpz_Init_Set_Ui (Rop => Zk (0),
                       Op  => 13968);
      Mpz_Init_Set_Ui (Rop => Zk (1),
                       Op  => 3022);
      Mpz_Init_Set_Ui (Rop => Zk (2),
                       Op  => 13781);
      Mpz_Init_Set_Ui (Rop => Zk (3),
                       Op  => 10866);

      Verify_Nth_Powers
        (Success  => Result,
         Err_Idx  => Index,
         Uk       => Uk,
         Ak       => Ak,
         Ek       => Ek,
         Zk       => Zk,
         N        => N);
      Assert (Condition => Result,
              Message   => "Verification failed");
      Assert (Condition => Index = -1,
              Message   => "Index mismatch" & Index'Img);

      Mpz_Clear (Integer => N);
      Mpz_Clear (Integers => Uk);
      Mpz_Clear (Integers => Ak);
      Mpz_Clear (Integers => Ek);
      Mpz_Clear (Integers => Zk);
   end Verify_Nth_Power_Valid;

end Paillier_Tests;
