--
--  Copyright (C) 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--  University of Applied Sciences Rapperswil
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ahven.Framework;
with Ahven.Text_Runner;

with Paillier_Tests;
with Random_Tests;
with OS_Tests;
with Election_Tests;
with Registry_Tests;
with Conversion_Tests;
with Tally_Tests;
with Instruction_Tests;

procedure Test_Runner is
   use Ahven.Framework;

   Name : constant String            := "Paillier/ZKP tests";
   S    : constant Test_Suite_Access := Create_Suite (Suite_Name => Name);
begin
   Add_Test (Suite => S.all,
             T     => new Paillier_Tests.Testcase);
   Add_Test (Suite => S.all,
             T     => new Random_Tests.Testcase);
   Add_Test (Suite => S.all,
             T     => new OS_Tests.Testcase);
   Add_Test (Suite => S.all,
             T     => new Election_Tests.Testcase);
   Add_Test (Suite => S.all,
             T     => new Registry_Tests.Testcase);
   Add_Test (Suite => S.all,
             T     => new Conversion_Tests.Testcase);
   Add_Test (Suite => S.all,
             T     => new Tally_Tests.Testcase);
   Add_Test (Suite => S.all,
             T     => new Instruction_Tests.Testcase);

   Ahven.Text_Runner.Run (Suite => S);
   Release_Suite (T => S);
end Test_Runner;
