--
--  Copyright (C) 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--  University of Applied Sciences Rapperswil
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ahven.Framework;

package Election_Tests is

   type Testcase is new Ahven.Framework.Test_Case with null record;

   procedure Initialize (T : in out Testcase);
   --  Initialize testcase.

   procedure Create_Election;
   --  Verify creation of election instances.

   procedure Load_Election;
   --  Verify loading of election from a file.

   procedure Election_To_JSON;
   --  Verify conversion of election to JSON representation.

   procedure Load_Ballot;
   --  Verify loading of ballot from a file.

   procedure Ballot_To_JSON;
   --  Verify conversion of ballot to JSON representation.

   procedure Check_Voting;
   --  Verify casting and verifying a vote.

   procedure Check_Voting_Cheat;
   --  Verify casting and verifying a vote (cheating).

   procedure Check_Voting_Invalid_Choice;
   --  Verify casting of a vote with an invalid choice.

   procedure Verify_Vote_Invalid_Commitment;
   --  Verify vote with invalid commitment.

end Election_Tests;
