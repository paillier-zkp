--
--  Copyright (C) 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--  University of Applied Sciences Rapperswil
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Strings.Unbounded;

package body ZKP.Elections.Mock is

   function U (S : String) return Ada.Strings.Unbounded.Unbounded_String
               renames Ada.Strings.Unbounded.To_Unbounded_String;

   -------------------------------------------------------------------------

   function Create_Ballot
     (Election_ID : String;
      Voter_ID    : String;
      Vote        : String;
      Ak          : String_Array;
      Ek          : String_Array;
      Zk          : String_Array)
      return Ballot_Type
   is
   begin
      return B : Ballot_Type (Choices => Ak'Length - 1) do
         B.Election_ID := U (Election_ID);
         B.Voter_ID    := U (Voter_ID);
         B.Vote        := U (Vote);
         B.Ak          := Ak;
         B.Ek          := Ek;
         B.Zk          := Zk;
      end return;
   end Create_Ballot;

end ZKP.Elections.Mock;
