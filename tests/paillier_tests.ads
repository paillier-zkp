--
--  Copyright (C) 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--  University of Applied Sciences Rapperswil
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ahven.Framework;

package Paillier_Tests is

   type Testcase is new Ahven.Framework.Test_Case with null record;

   procedure Initialize (T : in out Testcase);
   --  Initialize testcase.

   procedure Encrypt_Message;
   --  Verify message encryption.

   procedure Create_Messages_Mk;
   --  Test messages mk creation function.

   procedure Calculate_Ballots_Uk;
   --  Calculate voting ballots uk.

   procedure Calculate_Ballots_Uk_No_Inverse;
   --  Verify error behavior in case generator g has no inverse element in n^2.

   procedure Calculate_Commitment_Ak;
   --  Calculate commitment array ak.

   procedure Calculate_Commitment_Ak_Invalid_Choice;
   --  Calculate commitment array ak - invalid choice given.

   procedure Calculate_Commitment_Ak_No_Inverse;
   --  Calculate commitment array ak - no inverse element for uk^n in n^2.

   procedure Calculate_256_Bit_Challenge;
   --  Verify computation of a 256 bit challenge.

   procedure Calculate_768_Bit_Challenge;
   --  Verify computation of a 768 bit challenge.

   procedure Calculate_Response_Ek;
   --  Verify computation of response array ek.

   procedure Calculate_Response_Ek_Invalid_Choice;
   --  Calculate response array ek - invalid choice given.

   procedure Calculate_Response_Zk;
   --  Verify computation of response array zk.

   procedure Calculate_Response_Zk_Invalid_Choice;
   --  Calculate response array zk - invalid choice given.

   procedure Verify_Challenge_Equals_Eksum;
   --  Test response sum (ek) = challenge verification.

   procedure Verify_Nth_Power_Valid;
   --  Test n-th powers verification (valid proof).

   procedure Verify_Nth_Power_Cheat;
   --  Test n-th powers verification (someone cheated).

end Paillier_Tests;
