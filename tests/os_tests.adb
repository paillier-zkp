--
--  Copyright (C) 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--  University of Applied Sciences Rapperswil
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Directories;
with Ada.IO_Exceptions;

with ZKP.OS;

package body OS_Tests is

   use Ahven;
   use ZKP;

   Ref_File_Content : constant String := "Foobar" & ASCII.LF
     & "Some file content" & ASCII.LF;

   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "OS package tests");
      T.Add_Test_Routine
        (Routine => Read_File_Content'Access,
         Name    => "Read file content");
      T.Add_Test_Routine
        (Routine => Read_File_Content_Nonexistent'Access,
         Name    => "Read file content (non-existent)");
      T.Add_Test_Routine
        (Routine => Write_File_Content'Access,
         Name    => "Write file content");
   end Initialize;

   -------------------------------------------------------------------------

   procedure Read_File_Content
   is
      File : constant String := "data/testfile";
   begin
      Assert (Condition => OS.Read_File (Filename => File) = Ref_File_Content,
              Message   => "File content mismatch");
   end Read_File_Content;

   -------------------------------------------------------------------------

   procedure Read_File_Content_Nonexistent
   is
   begin
      begin
         declare
            C : constant String := OS.Read_File (Filename => "no/such/file");
            pragma Unreferenced (C);
         begin
            Fail (Message => "Exception expected");
         end;

      exception
         when Ada.IO_Exceptions.Name_Error => null;
      end;
   end Read_File_Content_Nonexistent;

   -------------------------------------------------------------------------

   procedure Write_File_Content
   is
      File : constant String := "obj/testfile";
   begin
      OS.Write_File (Content  => Ref_File_Content,
                     Filename => File);
      Assert (Condition => OS.Read_File (Filename => File) = Ref_File_Content,
              Message   => "File content mismatch");

      Ada.Directories.Delete_File (Name => File);

   exception
      when others =>
         if Ada.Directories.Exists (Name => File) then
            Ada.Directories.Delete_File (Name => File);
         end if;
         raise;
   end Write_File_Content;

end OS_Tests;
