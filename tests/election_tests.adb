--
--  Copyright (C) 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--  University of Applied Sciences Rapperswil
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Exceptions;
with Ada.Strings.Unbounded;

with ZKP.Elections.Mock;

package body Election_Tests is

   use Ada.Strings.Unbounded;
   use Ahven;
   use ZKP;
   use ZKP.Elections;

   function U (S : String) return Ada.Strings.Unbounded.Unbounded_String
               renames Ada.Strings.Unbounded.To_Unbounded_String;

   Ref_Election_File : constant String := "data/election_1536_1_2.json";
   --  Reference election file in JSON format.

   Ref_ID         : constant String := "Election 1536-1-2";
   Ref_Desc       : constant String := "Do you want to introduce e-voting?";
   Ref_Candidates : constant Candidates_Type
     := (U ("Yes"), U ("No"), U ("Neither"));
   Ref_N_Str      : constant String :=
     "c3ff274b33cee3ea2199969734fd4a6567d90459b2205e716f6ba535f03a256ffaf" &
   "adb5d01fc8be04d652010aeffc47b0834f6ed18f54bf6c23853f49d889c0d15c69394" &
   "01fb651fd3c28c7efebaeb3f54893dcac0f8ce8eb0166fa0f9d9f380a0e515c53ded7" &
   "05926f2b69262d3ab0e8ca0f494cfb593b7596fa9deb43922ab298c305eed2f7e860d" &
   "6bd5200b2b7dcc831c12dd61493b84d2aec2daff48744d7828829c7073410baf930e8" &
   "a32ca765debbe3ee12d97d2dcd64df6b3546c019f";
   Ref_G_Str      : constant String :=
     "c3ff274b33cee3ea2199969734fd4a6567d90459b2205e716f6ba535f03a256ffaf" &
   "adb5d01fc8be04d652010aeffc47b0834f6ed18f54bf6c23853f49d889c0d15c69394" &
   "01fb651fd3c28c7efebaeb3f54893dcac0f8ce8eb0166fa0f9d9f380a0e515c53ded7" &
   "05926f2b69262d3ab0e8ca0f494cfb593b7596fa9deb43922ab298c305eed2f7e860d" &
   "6bd5200b2b7dcc831c12dd61493b84d2aec2daff48744d7828829c7073410baf930e8" &
   "a32ca765debbe3ee12d97d2dcd64df6b3546c01a0";
   Ref_Exponent   : constant Positive := 32;
   Ref_JSON_Str   : constant String   :=
     "{"                                                                    &
   """n"":""c3ff274b33cee3ea2199969734fd4a6567d90459b2205e716f6ba535f03a25" &
   "6ffafadb5d01fc8be04d652010aeffc47b0834f6ed18f54bf6c23853f49d889c0d15c6" &
   "939401fb651fd3c28c7efebaeb3f54893dcac0f8ce8eb0166fa0f9d9f380a0e515c53d" &
   "ed705926f2b69262d3ab0e8ca0f494cfb593b7596fa9deb43922ab298c305eed2f7e86" &
   "0d6bd5200b2b7dcc831c12dd61493b84d2aec2daff48744d7828829c7073410baf930e" &
   "8a32ca765debbe3ee12d97d2dcd64df6b3546c019f"", "                         &
   """election_id"":""Election 1536-1-2"", "                                &
   """base_exponent"":32, "                                                 &
   """candidates_list"":[""Yes"", ""No"", ""Neither""], "                   &
   """election_description"":""Do you want to introduce e-voting?"", "      &
   """g"":""c3ff274b33cee3ea2199969734fd4a6567d90459b2205e716f6ba535f03a25" &
   "6ffafadb5d01fc8be04d652010aeffc47b0834f6ed18f54bf6c23853f49d889c0d15c6" &
   "939401fb651fd3c28c7efebaeb3f54893dcac0f8ce8eb0166fa0f9d9f380a0e515c53d" &
   "ed705926f2b69262d3ab0e8ca0f494cfb593b7596fa9deb43922ab298c305eed2f7e86" &
   "0d6bd5200b2b7dcc831c12dd61493b84d2aec2daff48744d7828829c7073410baf930e" &
   "8a32ca765debbe3ee12d97d2dcd64df6b3546c01a0""}";

   Ref_Ballot_File : constant String := "data/example_ballot.json";
   --  Reference ballot file in JSON format.

   Ref_V_ID        : constant String := "Arthur Dent";
   Ref_Vote_Str    : constant String :=
     "8d79595ed2f90c1dcaa1817d1f0725826e7cb7a6f04a678989f34535f252edbdfe59" &
   "851aa85ea468974ebb190e5065dbc8a9131e4a5b0c09175b41ed9e626b595ffc5d0cb9" &
   "10a928f69b4dee3a5752c4679a5977752b97b5d808335786ee473d668a564df93c37e7" &
   "8727c29a03464f79699d6d51ea32c7c349da2ca051bff62e4de88df1d4653828c6e036" &
   "a8cc52d96c28510e47a1a5cde719cc59f26cde270855ce6e6224f8de55eafa114f7c54" &
   "5ee1c728315b7b1a1f3bf577645fb4d6c5e2b68752c7daf7cee540146a8a3eb929431d" &
   "f13cacfd290e711ade3a6273f9badeed0b004f4f7f6cd00fe68307e4873d22e76b6811" &
   "19f0fe0131ad526e3de27559e14b04ec96ba5b9705929ac6f609fe35e659eaa905202a" &
   "e37a1cc0304adf8a3b9ae5921cb13d1623763a235ac2842ba3bab59832a5ade69b87fd" &
   "39b654cae5bf907ee4bfa430c038778f0f11e0f7befd1d16f9991c6fa3b9e6b8ac51cf" &
   "590d99863ed0d085fda799d857803a4c6ca8aa04b7ed2ecd2adda3b5f5fb8df21610d2";
   Ref_Ak_Str      : constant String_Array :=
     (0 => U ("8b35dc0a81c64a8b83bf56ba7f4163cacfc394557ec75a55b9cb7d135cc" &
      "684b40af17803973f97835a5e7f060447c7df0f548ebe4ee77e0c12334dab4bd794" &
      "7f3617ee14d4e992294860341dfc2f0dd571558038ce7cdec0fa4777847d1078139" &
      "7e86725f1334380712191b57ce44fd72211d43956ffeb6f260d370827be85a81efe" &
      "342ad41b2580dac5e64788aa0da3aa2671b671807de04227b684c007d6035bd5bf2" &
      "ae0f6c643a879b592073b5cf7dc879f4ebc8d85b420b32c86f20a696c97ca7c84b1" &
      "965a3d320a40338ac928105b41ce6f656f891efc50d62d31c2ff0aebdfba67047e6" &
      "cddaa3fa247094f1cf1c25451157372ec6249e14520db63a4b9b9828a0d7a9fb1dd" &
      "57414be53f8a683acd4d27f095050099d899c94c116cc3567209a059c5cb774fe11" &
      "d3dbef84b4b34091397f2d584ea02e0033ec888081642506afbe84a71f4b8325bc9" &
      "e899a79f64678993ad7de2dd31e198cd31c3acdbcebe8de52f579a6bea448b80650" &
      "76ede7c05e51a15e0427984d4f2666de5a94621"),
      1 => U ("6a72fe434a4ffc3ffd3b455fcd85724b7c558138dc91a98b8f4eb0c5c92" &
        "10545bc0f86d6fecccf91fc463d8e8386c14c28fa9c09649aa026ff3fdcd5e3b1" &
        "56838838a1d55053af5845cc58e86181120914f3fdf0d2bca6bf48112d30535c2" &
        "fe69750bd2e90efefa6850e49c91a92922ef3362d220c541d7fe659b3217b0578" &
        "fd64ee5e744dc864d38f8971701c86fbfba320eec5124adbc3292bc4a66b25751" &
        "d8ca94a3d267fdcc6498a2ae7dfaa8c03d5b10ccaeb889db58b4d717a9a4303b7" &
        "e04878bd16f27579f54576872de0e6db04ad1c6d7feb6b12e693d1a3c384f5c98" &
        "d9520c8b9cb57f01ae09f253a19bd2217aab8cb5e137ed1ab1dd033ff0381cbd0" &
        "98ac6bfce33c3b49657666cd9bc15e03938fb431828f3d806f17f748ac1e8dd68" &
        "de14ab10fab34c358e8970ad0cc878d1773e3234082a8841c1a34f2f4cc8d5173" &
        "d3cf312c3778ea932bc61afb349559c69bd5c8ac931a1df62c3c245c6a4f3c625" &
        "a2005a5bb64315238f25c8dc3e0287a63e3f2ae4024331823ec28130e01"),
      2 => U ("6dd658392eb0d3d7ffae21304fbca52f9a59b9ff37d3e993a628739d318" &
        "04bf777174c03a76803a7e5be510bd5b61ab099439f3c5ee4ca2240ef8b2b0632" &
        "7190e760250bccc19e925d905aadde787a4d57e022a14753b8d7790feeb00a735" &
        "7d98205fbeff06b2e3d21bedfa416a56699508b742bf8d855ff67de77fd31f52b" &
        "210cd7d3a6f87f9c7438e3f7b5f1ce6e9a670b4a7ffc9d41bb991e7954b606ac1" &
        "bf20ce3bb9d8aa02c7b5d091bbf13b410509c41e5777fe5fcb6fa01ec37a33553" &
        "da6f50635b39afecc46a32696c77614d43372002b404abd63cacb2367c948e992" &
        "92ba27bd1f6f8a22ac56896bde526b611bc03f311a112516e8945829d23a42c02" &
        "96f6a7868e7cfc165b49ab9adbf79863e64d9a6de4640c4a15a27a1fd19c02476" &
        "79770c2bd7dec66e7108c721d85ab8932200c622f8b20e0817ed6876063fbe1d3" &
        "e202696e25289ed634f89876a20c575228349a62934352f01fc1e49ac2c28106f" &
        "812397a95ad5f7f3f1f75075a2d113578f4fa22971a76d5676fae2c877a"));
   Ref_Ek_Str      : constant String_Array :=
     (0 => U ("2b6df071c47339b953bed7e3ba3129d92dfddaff806a432ba64712dafcb" &
      "896ab01617f34f14e993f169eda7314b26f09e191427e739c4a6241ea71bcb62e9e" &
      "77733174f04fe7ca296ec66e99186485aa2dfd1da88d945f9eaadf05f6162a740f"),
      1 => U ("dc27dfa0604c3ce35db27b4eef2514288ff6e05ff55a4dca4260458f2eb" &
        "bea462f82c4f30bda16817f9fc916096060e659f3ffa14a400b67ffea82375c15" &
        "2bef26123c70e4e89742d5c02c1043a0f6df8c9af9c9175abca190f2c8e3f1a8ba6"),
      2 => U ("7e2a70611d5cbb26b5a4ccd1551da9b48f8a59093b13f85d062d2245964" &
        "fb1dc963ae8422239b9afe37c23edcb652f8294403ca868917cebbe1ebf8dcf19" &
        "bffaa8833c2c75c8190cdf5c7823b860a75a9088820498f6cdb0501a60b36d850" &
        "89d"));
   Ref_Zk_Str      : constant String_Array :=
     (0 => U ("bb4cafd949859d154d5e167bb4300a739220be2e705734c22adf411a6e5" &
      "86fc72e2b9ac7455512067f773e8ee09920045ddfec6751e6c49b43d3c7cc3da1a7" &
      "3ce33f620fad42f4054ad661fb8be0a6a243cd972154c3014f6ffeb2fd876fcc569" &
      "fb48a10f7aa5f1e7653facbdf5f0200f9b7e3e545ba51c583c7b40d3a0b5efcdafd" &
      "7fd988b29db3eb0a72b06e2c096e55bbda34ccb4330b81421682199611351d689b0" &
      "8f1c00d344061fe739970500258f115c4e2aa5691e2085ea7aec27a4"),
      1 => U ("7e765b58b52d78b1856656ff9abc5ba628ee99d430a2b0991c5bdf51a5b" &
        "40b8a05c8748650dc6895786ee159464115d0c4e81d8ada36cd2fd631d2f1b2ee" &
        "8b66e4dc41ea7b763f08e0fb6fbec02cb39005b20f12e2899454265f214bbe12e" &
        "c2cf814d05146a90119913f170549f3ca123e04de50cd1468df43157d392dd3e3" &
        "bffbfdd156d98bb0ea9fb6d0cf7b2d50299f942a7aaa403ee03b80be13bd9bf77" &
        "7d300bd1cd2ab9c600cdd429e0177aa8bc8ecd44fc34c951e01b9f86119864ef5"),
      2 => U ("8ddcdc5f59673d52bf19cc1da21078e15ff9dc9d0649a648d63320160a9" &
        "c19d1ff9074b5fa8917653340eda8a5c44dce9e2fe3695d37268ba8a5a90cd2c4" &
        "1951e84828b6e7da5cf2d852424a5845c4d5f67abbd8165aa26c85d333781bd63" &
        "181b49da6aac6bfee6875e4be9be789a0d5b3235ccbdb4e4d74b54cf9c7c56b29" &
        "9be2e4806593445a516dff5df57853264a7b118ce94a43313348d7ae3eade2fe5" &
        "0fae980b0b73e4316f8f778bb02d175b1927d11c61932b969606bbb70fd4dece7"));

   Ref_Ballot_JSON_Str : constant String :=
     "{"                                                                    &
   """proof"":["                                                            &
   "{""z"":""bb4cafd949859d154d5e167bb4300a739220be2e705734c22adf411a6e586" &
   "fc72e2b9ac7455512067f773e8ee09920045ddfec6751e6c49b43d3c7cc3da1a73ce33" &
   "f620fad42f4054ad661fb8be0a6a243cd972154c3014f6ffeb2fd876fcc569fb48a10f" &
   "7aa5f1e7653facbdf5f0200f9b7e3e545ba51c583c7b40d3a0b5efcdafd7fd988b29db" &
   "3eb0a72b06e2c096e55bbda34ccb4330b81421682199611351d689b08f1c00d344061f" &
   "e739970500258f115c4e2aa5691e2085ea7aec27a4"", "                         &
   """a"":""8b35dc0a81c64a8b83bf56ba7f4163cacfc394557ec75a55b9cb7d135cc684" &
   "b40af17803973f97835a5e7f060447c7df0f548ebe4ee77e0c12334dab4bd7947f3617" &
   "ee14d4e992294860341dfc2f0dd571558038ce7cdec0fa4777847d10781397e86725f1" &
   "334380712191b57ce44fd72211d43956ffeb6f260d370827be85a81efe342ad41b2580" &
   "dac5e64788aa0da3aa2671b671807de04227b684c007d6035bd5bf2ae0f6c643a879b5" &
   "92073b5cf7dc879f4ebc8d85b420b32c86f20a696c97ca7c84b1965a3d320a40338ac9" &
   "28105b41ce6f656f891efc50d62d31c2ff0aebdfba67047e6cddaa3fa247094f1cf1c2" &
   "5451157372ec6249e14520db63a4b9b9828a0d7a9fb1dd57414be53f8a683acd4d27f0" &
   "95050099d899c94c116cc3567209a059c5cb774fe11d3dbef84b4b34091397f2d584ea" &
   "02e0033ec888081642506afbe84a71f4b8325bc9e899a79f64678993ad7de2dd31e198" &
   "cd31c3acdbcebe8de52f579a6bea448b8065076ede7c05e51a15e0427984d4f2666de5" &
   "a94621"", "                                                             &
   """e"":""2b6df071c47339b953bed7e3ba3129d92dfddaff806a432ba64712dafcb896" &
   "ab01617f34f14e993f169eda7314b26f09e191427e739c4a6241ea71bcb62e9e777331" &
   "74f04fe7ca296ec66e99186485aa2dfd1da88d945f9eaadf05f6162a740f""}, "      &
   "{""z"":""7e765b58b52d78b1856656ff9abc5ba628ee99d430a2b0991c5bdf51a5b40" &
   "b8a05c8748650dc6895786ee159464115d0c4e81d8ada36cd2fd631d2f1b2ee8b66e4d" &
   "c41ea7b763f08e0fb6fbec02cb39005b20f12e2899454265f214bbe12ec2cf814d0514" &
   "6a90119913f170549f3ca123e04de50cd1468df43157d392dd3e3bffbfdd156d98bb0e" &
   "a9fb6d0cf7b2d50299f942a7aaa403ee03b80be13bd9bf777d300bd1cd2ab9c600cdd4" &
   "29e0177aa8bc8ecd44fc34c951e01b9f86119864ef5"", "                        &
   """a"":""6a72fe434a4ffc3ffd3b455fcd85724b7c558138dc91a98b8f4eb0c5c92105" &
   "45bc0f86d6fecccf91fc463d8e8386c14c28fa9c09649aa026ff3fdcd5e3b156838838" &
   "a1d55053af5845cc58e86181120914f3fdf0d2bca6bf48112d30535c2fe69750bd2e90" &
   "efefa6850e49c91a92922ef3362d220c541d7fe659b3217b0578fd64ee5e744dc864d3" &
   "8f8971701c86fbfba320eec5124adbc3292bc4a66b25751d8ca94a3d267fdcc6498a2a" &
   "e7dfaa8c03d5b10ccaeb889db58b4d717a9a4303b7e04878bd16f27579f54576872de0" &
   "e6db04ad1c6d7feb6b12e693d1a3c384f5c98d9520c8b9cb57f01ae09f253a19bd2217" &
   "aab8cb5e137ed1ab1dd033ff0381cbd098ac6bfce33c3b49657666cd9bc15e03938fb4" &
   "31828f3d806f17f748ac1e8dd68de14ab10fab34c358e8970ad0cc878d1773e3234082" &
   "a8841c1a34f2f4cc8d5173d3cf312c3778ea932bc61afb349559c69bd5c8ac931a1df6" &
   "2c3c245c6a4f3c625a2005a5bb64315238f25c8dc3e0287a63e3f2ae4024331823ec28" &
   "130e01"", "                                                             &
   """e"":""dc27dfa0604c3ce35db27b4eef2514288ff6e05ff55a4dca4260458f2ebbea" &
   "462f82c4f30bda16817f9fc916096060e659f3ffa14a400b67ffea82375c152bef2612" &
   "3c70e4e89742d5c02c1043a0f6df8c9af9c9175abca190f2c8e3f1a8ba6""}, "       &
   "{""z"":""8ddcdc5f59673d52bf19cc1da21078e15ff9dc9d0649a648d63320160a9c1" &
   "9d1ff9074b5fa8917653340eda8a5c44dce9e2fe3695d37268ba8a5a90cd2c41951e84" &
   "828b6e7da5cf2d852424a5845c4d5f67abbd8165aa26c85d333781bd63181b49da6aac" &
   "6bfee6875e4be9be789a0d5b3235ccbdb4e4d74b54cf9c7c56b299be2e4806593445a5" &
   "16dff5df57853264a7b118ce94a43313348d7ae3eade2fe50fae980b0b73e4316f8f77" &
   "8bb02d175b1927d11c61932b969606bbb70fd4dece7"", "                        &
   """a"":""6dd658392eb0d3d7ffae21304fbca52f9a59b9ff37d3e993a628739d31804b" &
   "f777174c03a76803a7e5be510bd5b61ab099439f3c5ee4ca2240ef8b2b06327190e760" &
   "250bccc19e925d905aadde787a4d57e022a14753b8d7790feeb00a7357d98205fbeff0" &
   "6b2e3d21bedfa416a56699508b742bf8d855ff67de77fd31f52b210cd7d3a6f87f9c74" &
   "38e3f7b5f1ce6e9a670b4a7ffc9d41bb991e7954b606ac1bf20ce3bb9d8aa02c7b5d09" &
   "1bbf13b410509c41e5777fe5fcb6fa01ec37a33553da6f50635b39afecc46a32696c77" &
   "614d43372002b404abd63cacb2367c948e99292ba27bd1f6f8a22ac56896bde526b611" &
   "bc03f311a112516e8945829d23a42c0296f6a7868e7cfc165b49ab9adbf79863e64d9a" &
   "6de4640c4a15a27a1fd19c0247679770c2bd7dec66e7108c721d85ab8932200c622f8b" &
   "20e0817ed6876063fbe1d3e202696e25289ed634f89876a20c575228349a62934352f0" &
   "1fc1e49ac2c28106f812397a95ad5f7f3f1f75075a2d113578f4fa22971a76d5676fae" &
   "2c877a"", "                                                             &
   """e"":""7e2a70611d5cbb26b5a4ccd1551da9b48f8a59093b13f85d062d2245964fb1" &
   "dc963ae8422239b9afe37c23edcb652f8294403ca868917cebbe1ebf8dcf19bffaa883" &
   "3c2c75c8190cdf5c7823b860a75a9088820498f6cdb0501a60b36d85089d""}], "     &
   """election_id"":""Election 1536-1-2"", "                                &
   """ballot"":""8d79595ed2f90c1dcaa1817d1f0725826e7cb7a6f04a678989f34535f" &
   "252edbdfe59851aa85ea468974ebb190e5065dbc8a9131e4a5b0c09175b41ed9e626b5" &
   "95ffc5d0cb910a928f69b4dee3a5752c4679a5977752b97b5d808335786ee473d668a5" &
   "64df93c37e78727c29a03464f79699d6d51ea32c7c349da2ca051bff62e4de88df1d46" &
   "53828c6e036a8cc52d96c28510e47a1a5cde719cc59f26cde270855ce6e6224f8de55e" &
   "afa114f7c545ee1c728315b7b1a1f3bf577645fb4d6c5e2b68752c7daf7cee540146a8" &
   "a3eb929431df13cacfd290e711ade3a6273f9badeed0b004f4f7f6cd00fe68307e4873" &
   "d22e76b681119f0fe0131ad526e3de27559e14b04ec96ba5b9705929ac6f609fe35e65" &
   "9eaa905202ae37a1cc0304adf8a3b9ae5921cb13d1623763a235ac2842ba3bab59832a" &
   "5ade69b87fd39b654cae5bf907ee4bfa430c038778f0f11e0f7befd1d16f9991c6fa3b" &
   "9e6b8ac51cf590d99863ed0d085fda799d857803a4c6ca8aa04b7ed2ecd2adda3b5f5f" &
   "b8df21610d2"", "                                                        &
   """voter_id"":""Arthur Dent""}";

   -------------------------------------------------------------------------

   procedure Ballot_To_JSON
   is
      B : constant Ballot_Type := Load_File
        (Filename => "data/example_ballot.json");
   begin
      Assert (Condition => To_JSON_String (Ballot => B) = Ref_Ballot_JSON_Str,
              Message   => "Ballot JSON string mismatch");
   end Ballot_To_JSON;

   -------------------------------------------------------------------------

   procedure Check_Voting
   is
      E : constant Election_Type := Load_File (Filename => Ref_Election_File);
      B : constant Ballot_Type   := Cast_Vote (Election => E,
                                               Voter_ID => Ref_V_ID,
                                               Choice   => 1);
   begin
      Assert (Condition => Verify_Vote
              (Election => E,
               Ballot   => B),
              Message   => "Vote verification failed");
   end Check_Voting;

   -------------------------------------------------------------------------

   procedure Check_Voting_Cheat
   is
      E : constant Election_Type := Load_File (Filename => Ref_Election_File);
      B : constant Ballot_Type   := Cast_Vote (Election => E,
                                               Voter_ID => Ref_V_ID,
                                               Choice   => 1,
                                               Cheat    => True);
   begin
      Assert (Condition => not Verify_Vote
              (Election => E,
               Ballot   => B),
              Message   => "Cheating not detected");
   end Check_Voting_Cheat;

   -------------------------------------------------------------------------

   procedure Check_Voting_Invalid_Choice
   is
      use Ada.Exceptions;

      Ref_Msg : constant String
        := "Choice 42 is invalid: must be in range 1 .. 2";
      E       : constant Election_Type
        := Load_File (Filename => Ref_Election_File);
   begin
      declare
         B : constant Ballot_Type
           := Cast_Vote (Election => E,
                         Voter_ID => Ref_V_ID,
                         Choice   => 42);
         pragma Unreferenced (B);
      begin
         Fail (Message => "Exception expected");
      end;

   exception
      when E : Invalid_Election =>
         Assert (Condition => Exception_Message (X => E) = Ref_Msg,
                 Message   => "Exception message mismatch");
   end Check_Voting_Invalid_Choice;

   -------------------------------------------------------------------------

   procedure Create_Election
   is
      E : constant Election_Type := Create
        (ID          => Ref_ID,
         Description => Ref_Desc,
         Candidates  => Ref_Candidates,
         N           => Ref_N_Str,
         G           => Ref_G_Str,
         Exponent    => Ref_Exponent);
   begin
      Assert (Condition => Get_ID (Election => E) = Ref_ID,
              Message   => "Election ID mismatch");
      Assert (Condition => Get_Description (Election => E) = Ref_Desc,
              Message   => "Election description mismatch");
      Assert (Condition => Get_Candidates (Election => E) = Ref_Candidates,
              Message   => "Election candidates mismatch");
      Assert (Condition => Get_N (Election => E) = Ref_N_Str,
              Message   => "Election modulus (N) mismatch");
      Assert (Condition => Get_G (Election => E) = Ref_G_Str,
              Message   => "Election generator (G) mismatch");
      Assert (Condition => Get_Exponent (Election => E) = Ref_Exponent,
              Message   => "Election exponent mismatch");
   end Create_Election;

   -------------------------------------------------------------------------

   procedure Election_To_JSON
   is
      E : constant Election_Type := Create
        (ID          => Ref_ID,
         Description => Ref_Desc,
         Candidates  => Ref_Candidates,
         N           => Ref_N_Str,
         G           => Ref_G_Str,
         Exponent    => Ref_Exponent);
   begin
      Assert (Condition => To_JSON_String (Election => E) = Ref_JSON_Str,
              Message   => "JSON string mismatch");
   end Election_To_JSON;

   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "Election type related tests");
      T.Add_Test_Routine
        (Routine => Create_Election'Access,
         Name    => "Create election instance");
      T.Add_Test_Routine
        (Routine => Load_Election'Access,
         Name    => "Load election from file");
      T.Add_Test_Routine
        (Routine => Election_To_JSON'Access,
         Name    => "Election to JSON conversion");
      T.Add_Test_Routine
        (Routine => Load_Ballot'Access,
         Name    => "Load ballot from file");
      T.Add_Test_Routine
        (Routine => Ballot_To_JSON'Access,
         Name    => "Ballot to JSON conversion");
      T.Add_Test_Routine
        (Routine => Check_Voting'Access,
         Name    => "Voting & Verification");
      T.Add_Test_Routine
        (Routine => Check_Voting_Cheat'Access,
         Name    => "Voting & Verification (cheat)");
      T.Add_Test_Routine
        (Routine => Check_Voting_Invalid_Choice'Access,
         Name    => "Voting (invalid choice)");
      T.Add_Test_Routine
        (Routine => Verify_Vote_Invalid_Commitment'Access,
         Name    => "Verify vote (invalid commitment)");
   end Initialize;

   -------------------------------------------------------------------------

   procedure Load_Ballot
   is
      B : constant Ballot_Type := Load_File (Filename => Ref_Ballot_File);
   begin
      Assert (Condition => Get_Election_ID (Ballot => B) = Ref_ID,
              Message   => "Election ID mismatch");
      Assert (Condition => Get_Voter_ID (Ballot => B) = Ref_V_ID,
              Message   => "Voter ID mismatch");
      Assert (Condition => Get_Vote (Ballot => B) = Ref_Vote_Str,
              Message   => "Vote string mismatch");
      Assert (Condition => Get_Vote (Ballot => B) = Ref_Vote_Str,
              Message   => "Vote string mismatch");
      Assert (Condition => Get_Vote (Ballot => B) = Ref_Vote_Str,
              Message   => "Vote string mismatch");

      declare
         Ak : constant String_Array := Get_Ak (Ballot => B);
         Ek : constant String_Array := Get_Ek (Ballot => B);
         Zk : constant String_Array := Get_Zk (Ballot => B);
      begin
         for I in Ak'Range loop
            Assert (Condition => Ak (I) = Ref_Ak_Str (I),
                    Message   => "Ak mismatch:" & I'Img);
            Assert (Condition => Ek (I) = Ref_Ek_Str (I),
                    Message   => "Ek mismatch:" & I'Img);
            Assert (Condition => Zk (I) = Ref_Zk_Str (I),
                    Message   => "Zk mismatch:" & I'Img);
         end loop;
      end;
   end Load_Ballot;

   -------------------------------------------------------------------------

   procedure Load_Election
   is
      E : constant Election_Type
        := Load_File (Filename => Ref_Election_File);
   begin
      Assert (Condition => Get_ID (Election => E) = Ref_ID,
              Message   => "Election ID mismatch");
      Assert (Condition => Get_Description (Election => E) = Ref_Desc,
              Message   => "Election description mismatch");
      Assert (Condition => Get_Candidates (Election => E) = Ref_Candidates,
              Message   => "Election candidates mismatch");
      Assert (Condition => Get_N (Election => E) = Ref_N_Str,
              Message   => "Election modulus (N) mismatch");
      Assert (Condition => Get_G (Election => E) = Ref_G_Str,
              Message   => "Election generator (G) mismatch");
      Assert (Condition => Get_Exponent (Election => E) = Ref_Exponent,
              Message   => "Election exponent mismatch");
   end Load_Election;

   -------------------------------------------------------------------------

   procedure Verify_Vote_Invalid_Commitment
   is
      E : constant Election_Type := Load_File (Filename => Ref_Election_File);
      B : constant Ballot_Type   := Elections.Mock.Create_Ballot
        (Election_ID => Ref_ID,
         Voter_ID    => Ref_V_ID,
         Vote        => Ref_Vote_Str,
         Ak          => (0 => U ("0"), 1 => U ("0"), 2 => U ("0")),
         Ek          => Ref_Ek_Str,
         Zk          => Ref_Zk_Str);
   begin
      Assert (Condition => not Verify_Vote
              (Election => E,
               Ballot   => B),
              Message   => "Vote verification succeded");
   end Verify_Vote_Invalid_Commitment;

end Election_Tests;
